<?php
class FluidCache_Standalone_partial_Header_html_9e45c7527f47a380a0b2fc441b06ea1073b38059 extends \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate {

public function getVariableContainer() {
	// @todo
	return new \TYPO3\CMS\Fluid\Core\ViewHelper\TemplateVariableContainer();
}
public function getLayoutName(\TYPO3\CMS\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();

return NULL;
}
public function hasLayout() {
return FALSE;
}

/**
 * Main Render function
 */
public function render(\TYPO3\CMS\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();

$output0 = '';

$output0 .= '

<header>
    <div class="row hidden-xs">
        <div class="col-xs-12 col-sm-4 col-md-3 social-buttons">
            <div class="margin-b btn-group btn-group-justified btn-group-lg social-media-list" role="group">
                ';
// Rendering ViewHelper FluidTYPO3\Vhs\ViewHelpers\Page\MenuViewHelper
$arguments1 = array();
$arguments1['pageUid'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('settings'), 'pidSocial', $renderingContext);
$arguments1['entryLevel'] = '0';
// Rendering Boolean node
$arguments1['expandAll'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('TRUE');
$arguments1['levels'] = '3';
// Rendering Boolean node
$arguments1['showHiddenInMenu'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('FALSE');
$arguments1['as'] = 'socialMenu';
$arguments1['classCurrent'] = '';
$arguments1['classActive'] = 'active';
$arguments1['classHasSubpages'] = 'hasSub';
$arguments1['additionalAttributes'] = NULL;
$arguments1['data'] = NULL;
$arguments1['class'] = NULL;
$arguments1['dir'] = NULL;
$arguments1['id'] = NULL;
$arguments1['lang'] = NULL;
$arguments1['style'] = NULL;
$arguments1['title'] = NULL;
$arguments1['accesskey'] = NULL;
$arguments1['tabindex'] = NULL;
$arguments1['onclick'] = NULL;
$arguments1['forceClosingTag'] = false;
$arguments1['hideIfEmpty'] = false;
$arguments1['contenteditable'] = NULL;
$arguments1['contextmenu'] = NULL;
$arguments1['draggable'] = NULL;
$arguments1['dropzone'] = NULL;
$arguments1['translate'] = NULL;
$arguments1['spellcheck'] = NULL;
$arguments1['hidden'] = NULL;
$arguments1['tagName'] = 'ul';
$arguments1['tagNameChildren'] = 'li';
$arguments1['divider'] = NULL;
$arguments1['useShortcutUid'] = false;
$arguments1['useShortcutTarget'] = NULL;
$arguments1['useShortcutData'] = NULL;
$arguments1['classFirst'] = '';
$arguments1['classLast'] = '';
$arguments1['substElementUid'] = '';
$arguments1['includeSpacers'] = false;
$arguments1['resolveExclude'] = false;
$arguments1['showHidden'] = false;
$arguments1['showCurrent'] = true;
$arguments1['linkCurrent'] = true;
$arguments1['linkActive'] = true;
$arguments1['titleFields'] = 'nav_title,title';
$arguments1['doktypes'] = NULL;
$arguments1['excludeSubpageTypes'] = 'SYSFOLDER';
$arguments1['deferred'] = false;
$arguments1['rootLineAs'] = 'rootLine';
$arguments1['excludePages'] = '';
$arguments1['includeAnchorTitle'] = true;
$arguments1['forceAbsoluteUrl'] = false;
$renderChildrenClosure2 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output3 = '';

$output3 .= '
                    ';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ForViewHelper
$arguments4 = array();
$arguments4['each'] = $currentVariableContainer->getOrNull('socialMenu');
$arguments4['as'] = 'socialMenuItem';
$arguments4['iteration'] = 'iterator';
$arguments4['key'] = '';
$arguments4['reverse'] = false;
$renderChildrenClosure5 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output6 = '';

$output6 .= '
                        ';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Link\ExternalViewHelper
$arguments7 = array();
$arguments7['class'] = 'btn btn-link';
$arguments7['uri'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('socialMenuItem'), 'url', $renderingContext);
$arguments7['additionalAttributes'] = NULL;
$arguments7['data'] = NULL;
$arguments7['defaultScheme'] = 'http';
$arguments7['dir'] = NULL;
$arguments7['id'] = NULL;
$arguments7['lang'] = NULL;
$arguments7['style'] = NULL;
$arguments7['title'] = NULL;
$arguments7['accesskey'] = NULL;
$arguments7['tabindex'] = NULL;
$arguments7['onclick'] = NULL;
$arguments7['name'] = NULL;
$arguments7['rel'] = NULL;
$arguments7['rev'] = NULL;
$arguments7['target'] = NULL;
$renderChildrenClosure8 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output9 = '';

$output9 .= '<i class="fa fa-2x fa-';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments10 = array();
$arguments10['value'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('socialMenuItem'), 'title', $renderingContext);
$arguments10['keepQuotes'] = false;
$arguments10['encoding'] = NULL;
$arguments10['doubleEncode'] = true;
$renderChildrenClosure11 = function() {return NULL;};
$value12 = ($arguments10['value'] !== NULL ? $arguments10['value'] : $renderChildrenClosure11());

$output9 .= (!is_string($value12) ? $value12 : htmlspecialchars($value12, ($arguments10['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments10['encoding'] !== NULL ? $arguments10['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments10['doubleEncode']));

$output9 .= '"></i>';
return $output9;
};
$viewHelper13 = $self->getViewHelper('$viewHelper13', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Link\ExternalViewHelper');
$viewHelper13->setArguments($arguments7);
$viewHelper13->setRenderingContext($renderingContext);
$viewHelper13->setRenderChildrenClosure($renderChildrenClosure8);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Link\ExternalViewHelper

$output6 .= $viewHelper13->initializeArgumentsAndRender();

$output6 .= '
                    ';
return $output6;
};

$output3 .= TYPO3\CMS\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments4, $renderChildrenClosure5, $renderingContext);

$output3 .= '
                ';
return $output3;
};
$viewHelper14 = $self->getViewHelper('$viewHelper14', $renderingContext, 'FluidTYPO3\Vhs\ViewHelpers\Page\MenuViewHelper');
$viewHelper14->setArguments($arguments1);
$viewHelper14->setRenderingContext($renderingContext);
$viewHelper14->setRenderChildrenClosure($renderChildrenClosure2);
// End of ViewHelper FluidTYPO3\Vhs\ViewHelpers\Page\MenuViewHelper

$output0 .= $viewHelper14->initializeArgumentsAndRender();

$output0 .= '
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-xs-12 brand">
            <div class="col-xs-2 visible-xs pull-right">
                <button type="button" class="collapsed btn btn-link" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <i class="fa fa-2x fa-bars"></i>
                </button>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-10">
                ';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Link\TypolinkViewHelper
$arguments15 = array();
$arguments15['parameter'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('settings'), 'pidHome', $renderingContext);
$arguments15['target'] = '';
$arguments15['class'] = '';
$arguments15['title'] = '';
$arguments15['additionalParams'] = '';
$arguments15['additionalAttributes'] = array (
);
$renderChildrenClosure16 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
return '
                    <img src="fileadmin/Images/wolldeern_500x1301_heller.png" class="margin-b img-responsive img-center" />
                ';
};

$output0 .= TYPO3\CMS\Fluid\ViewHelpers\Link\TypolinkViewHelper::renderStatic($arguments15, $renderChildrenClosure16, $renderingContext);

$output0 .= '
            </div>
        </div>
    </div>
    <div class="row">
        <nav id="navbar" class="navbar navbar-collapse collapse" role="navigation">
            <ul class="nav navbar-nav">
                ';
// Rendering ViewHelper FluidTYPO3\Vhs\ViewHelpers\Page\MenuViewHelper
$arguments17 = array();
$arguments17['pageUid'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('settings'), 'pidPrimary', $renderingContext);
$arguments17['entryLevel'] = '0';
// Rendering Boolean node
$arguments17['expandAll'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('TRUE');
$arguments17['levels'] = '3';
// Rendering Boolean node
$arguments17['showHiddenInMenu'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('FALSE');
$arguments17['as'] = 'menu';
$arguments17['classCurrent'] = '';
$arguments17['classActive'] = 'active';
$arguments17['classHasSubpages'] = 'hasSub';
$arguments17['additionalAttributes'] = NULL;
$arguments17['data'] = NULL;
$arguments17['class'] = NULL;
$arguments17['dir'] = NULL;
$arguments17['id'] = NULL;
$arguments17['lang'] = NULL;
$arguments17['style'] = NULL;
$arguments17['title'] = NULL;
$arguments17['accesskey'] = NULL;
$arguments17['tabindex'] = NULL;
$arguments17['onclick'] = NULL;
$arguments17['forceClosingTag'] = false;
$arguments17['hideIfEmpty'] = false;
$arguments17['contenteditable'] = NULL;
$arguments17['contextmenu'] = NULL;
$arguments17['draggable'] = NULL;
$arguments17['dropzone'] = NULL;
$arguments17['translate'] = NULL;
$arguments17['spellcheck'] = NULL;
$arguments17['hidden'] = NULL;
$arguments17['tagName'] = 'ul';
$arguments17['tagNameChildren'] = 'li';
$arguments17['divider'] = NULL;
$arguments17['useShortcutUid'] = false;
$arguments17['useShortcutTarget'] = NULL;
$arguments17['useShortcutData'] = NULL;
$arguments17['classFirst'] = '';
$arguments17['classLast'] = '';
$arguments17['substElementUid'] = '';
$arguments17['includeSpacers'] = false;
$arguments17['resolveExclude'] = false;
$arguments17['showHidden'] = false;
$arguments17['showCurrent'] = true;
$arguments17['linkCurrent'] = true;
$arguments17['linkActive'] = true;
$arguments17['titleFields'] = 'nav_title,title';
$arguments17['doktypes'] = NULL;
$arguments17['excludeSubpageTypes'] = 'SYSFOLDER';
$arguments17['deferred'] = false;
$arguments17['rootLineAs'] = 'rootLine';
$arguments17['excludePages'] = '';
$arguments17['includeAnchorTitle'] = true;
$arguments17['forceAbsoluteUrl'] = false;
$renderChildrenClosure18 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output19 = '';

$output19 .= '
                    <li role="presentation" class="';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments20 = array();
$arguments20['value'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('menu'), 'class', $renderingContext);
$arguments20['keepQuotes'] = false;
$arguments20['encoding'] = NULL;
$arguments20['doubleEncode'] = true;
$renderChildrenClosure21 = function() {return NULL;};
$value22 = ($arguments20['value'] !== NULL ? $arguments20['value'] : $renderChildrenClosure21());

$output19 .= (!is_string($value22) ? $value22 : htmlspecialchars($value22, ($arguments20['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments20['encoding'] !== NULL ? $arguments20['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments20['doubleEncode']));

$output19 .= '">';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Link\TypolinkViewHelper
$arguments23 = array();
$arguments23['parameter'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('settings'), 'pidHome', $renderingContext);
$arguments23['target'] = '';
$arguments23['class'] = '';
$arguments23['title'] = '';
$arguments23['additionalParams'] = '';
$arguments23['additionalAttributes'] = array (
);
$renderChildrenClosure24 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments25 = array();
$arguments25['value'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('settings'), 'pidHome.title', $renderingContext);
$arguments25['keepQuotes'] = false;
$arguments25['encoding'] = NULL;
$arguments25['doubleEncode'] = true;
$renderChildrenClosure26 = function() {return NULL;};
$value27 = ($arguments25['value'] !== NULL ? $arguments25['value'] : $renderChildrenClosure26());
return (!is_string($value27) ? $value27 : htmlspecialchars($value27, ($arguments25['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments25['encoding'] !== NULL ? $arguments25['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments25['doubleEncode']));
};

$output19 .= TYPO3\CMS\Fluid\ViewHelpers\Link\TypolinkViewHelper::renderStatic($arguments23, $renderChildrenClosure24, $renderingContext);

$output19 .= '</li>
                    ';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ForViewHelper
$arguments28 = array();
$arguments28['each'] = $currentVariableContainer->getOrNull('menu');
$arguments28['as'] = 'menuItem';
$arguments28['key'] = '';
$arguments28['reverse'] = false;
$arguments28['iteration'] = NULL;
$renderChildrenClosure29 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output30 = '';

$output30 .= '
                        <li role="presentation" class="';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments31 = array();
$arguments31['value'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('menuItem'), 'class', $renderingContext);
$arguments31['keepQuotes'] = false;
$arguments31['encoding'] = NULL;
$arguments31['doubleEncode'] = true;
$renderChildrenClosure32 = function() {return NULL;};
$value33 = ($arguments31['value'] !== NULL ? $arguments31['value'] : $renderChildrenClosure32());

$output30 .= (!is_string($value33) ? $value33 : htmlspecialchars($value33, ($arguments31['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments31['encoding'] !== NULL ? $arguments31['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments31['doubleEncode']));

$output30 .= '">
                            ';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Link\TypolinkViewHelper
$arguments34 = array();
$arguments34['parameter'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('menuItem'), 'uid', $renderingContext);
$arguments34['target'] = '';
$arguments34['class'] = '';
$arguments34['title'] = '';
$arguments34['additionalParams'] = '';
$arguments34['additionalAttributes'] = array (
);
$renderChildrenClosure35 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments36 = array();
$arguments36['value'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('menuItem'), 'title', $renderingContext);
$arguments36['keepQuotes'] = false;
$arguments36['encoding'] = NULL;
$arguments36['doubleEncode'] = true;
$renderChildrenClosure37 = function() {return NULL;};
$value38 = ($arguments36['value'] !== NULL ? $arguments36['value'] : $renderChildrenClosure37());
return (!is_string($value38) ? $value38 : htmlspecialchars($value38, ($arguments36['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments36['encoding'] !== NULL ? $arguments36['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments36['doubleEncode']));
};

$output30 .= TYPO3\CMS\Fluid\ViewHelpers\Link\TypolinkViewHelper::renderStatic($arguments34, $renderChildrenClosure35, $renderingContext);

$output30 .= '
                        </li>
                    ';
return $output30;
};

$output19 .= TYPO3\CMS\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments28, $renderChildrenClosure29, $renderingContext);

$output19 .= '
                ';
return $output19;
};
$viewHelper39 = $self->getViewHelper('$viewHelper39', $renderingContext, 'FluidTYPO3\Vhs\ViewHelpers\Page\MenuViewHelper');
$viewHelper39->setArguments($arguments17);
$viewHelper39->setRenderingContext($renderingContext);
$viewHelper39->setRenderChildrenClosure($renderChildrenClosure18);
// End of ViewHelper FluidTYPO3\Vhs\ViewHelpers\Page\MenuViewHelper

$output0 .= $viewHelper39->initializeArgumentsAndRender();

$output0 .= '
            </ul>
            <div class="hidden-xs pull-right collapse absolute" id="search-form">
                <form action="';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Uri\PageViewHelper
$arguments40 = array();
$arguments40['pageUid'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('settings'), 'pidSearch', $renderingContext);
$arguments40['additionalParams'] = array (
);
$arguments40['pageType'] = 0;
$arguments40['noCache'] = false;
$arguments40['noCacheHash'] = false;
$arguments40['section'] = '';
$arguments40['linkAccessRestrictedPages'] = false;
$arguments40['absolute'] = false;
$arguments40['addQueryString'] = false;
$arguments40['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments40['addQueryStringMethod'] = NULL;
$renderChildrenClosure41 = function() {return NULL;};

$output0 .= TYPO3\CMS\Fluid\ViewHelpers\Uri\PageViewHelper::renderStatic($arguments40, $renderChildrenClosure41, $renderingContext);

$output0 .= '" method="post">
                    <input type="text" name="tx_indexedsearch[sword]" class="form-control">
                </form>
            </div>
            <a id="search-toggle" class="btn btn-link collapsed" role="button" data-toggle="collapse" href="#search-form" aria-expanded="false" aria-controls="search-form">
                <i class="fa fa-2x fa-search"></i>
            </a>
        </nav>
    </div>
</header>';


return $output0;
}


}
#1468514224    18934     