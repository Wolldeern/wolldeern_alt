<?php
class FluidCache_Fluid_ViewHelpers_Widget_Paginate_action_index_27c78f00a4c3cd914ef32674ac11afab97a76f20 extends \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate {

public function getVariableContainer() {
	// @todo
	return new \TYPO3\CMS\Fluid\Core\ViewHelper\TemplateVariableContainer();
}
public function getLayoutName(\TYPO3\CMS\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();

return NULL;
}
public function hasLayout() {
return FALSE;
}

/**
 * section paginator
 */
public function section_31b8d545b1939b065e8931304bab52b99d8b4567(\TYPO3\CMS\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();

$output0 = '';

$output0 .= '
	<nav>
		<ul class="pagination">
			';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments1 = array();
// Rendering Boolean node
$arguments1['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('pagination'), 'previousPage', $renderingContext));
$arguments1['then'] = NULL;
$arguments1['else'] = NULL;
$renderChildrenClosure2 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output3 = '';

$output3 .= '
				<li class="previous">
					';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments4 = array();
// Rendering Boolean node
$arguments4['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('>', \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('pagination'), 'previousPage', $renderingContext), 1);
$arguments4['then'] = NULL;
$arguments4['else'] = NULL;
$renderChildrenClosure5 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output6 = '';

$output6 .= '
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper
$arguments7 = array();
$renderChildrenClosure8 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output9 = '';

$output9 .= '
							';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments10 = array();
// Rendering Array
$array11 = array();
$array11['currentPage'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('pagination'), 'previousPage', $renderingContext);
$arguments10['arguments'] = $array11;
$arguments10['addQueryStringMethod'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'addQueryStringMethod', $renderingContext);
$arguments10['section'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'section', $renderingContext);
// Rendering Array
$array12 = array();
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments13 = array();
$arguments13['key'] = 'widget.pagination.previous';
$arguments13['id'] = NULL;
$arguments13['default'] = NULL;
$arguments13['htmlEscape'] = NULL;
$arguments13['arguments'] = NULL;
$arguments13['extensionName'] = NULL;
$renderChildrenClosure14 = function() {return NULL;};
$array12['aria-label'] = TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper::renderStatic($arguments13, $renderChildrenClosure14, $renderingContext);
$arguments10['additionalAttributes'] = $array12;
$arguments10['data'] = NULL;
$arguments10['action'] = NULL;
$arguments10['format'] = '';
$arguments10['ajax'] = false;
$arguments10['class'] = NULL;
$arguments10['dir'] = NULL;
$arguments10['id'] = NULL;
$arguments10['lang'] = NULL;
$arguments10['style'] = NULL;
$arguments10['title'] = NULL;
$arguments10['accesskey'] = NULL;
$arguments10['tabindex'] = NULL;
$arguments10['onclick'] = NULL;
$arguments10['name'] = NULL;
$arguments10['rel'] = NULL;
$arguments10['rev'] = NULL;
$arguments10['target'] = NULL;
$renderChildrenClosure15 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
return '
								<span aria-hidden="true">&laquo;</span>
							';
};
$viewHelper16 = $self->getViewHelper('$viewHelper16', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper16->setArguments($arguments10);
$viewHelper16->setRenderingContext($renderingContext);
$viewHelper16->setRenderChildrenClosure($renderChildrenClosure15);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output9 .= $viewHelper16->initializeArgumentsAndRender();

$output9 .= '
						';
return $output9;
};

$output6 .= TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper::renderStatic($arguments7, $renderChildrenClosure8, $renderingContext);

$output6 .= '
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper
$arguments17 = array();
$renderChildrenClosure18 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output19 = '';

$output19 .= '
							';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments20 = array();
$arguments20['addQueryStringMethod'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'addQueryStringMethod', $renderingContext);
$arguments20['section'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'section', $renderingContext);
// Rendering Array
$array21 = array();
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments22 = array();
$arguments22['key'] = 'widget.pagination.previous';
$arguments22['id'] = NULL;
$arguments22['default'] = NULL;
$arguments22['htmlEscape'] = NULL;
$arguments22['arguments'] = NULL;
$arguments22['extensionName'] = NULL;
$renderChildrenClosure23 = function() {return NULL;};
$array21['aria-label'] = TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper::renderStatic($arguments22, $renderChildrenClosure23, $renderingContext);
$arguments20['additionalAttributes'] = $array21;
$arguments20['data'] = NULL;
$arguments20['action'] = NULL;
$arguments20['arguments'] = array (
);
$arguments20['format'] = '';
$arguments20['ajax'] = false;
$arguments20['class'] = NULL;
$arguments20['dir'] = NULL;
$arguments20['id'] = NULL;
$arguments20['lang'] = NULL;
$arguments20['style'] = NULL;
$arguments20['title'] = NULL;
$arguments20['accesskey'] = NULL;
$arguments20['tabindex'] = NULL;
$arguments20['onclick'] = NULL;
$arguments20['name'] = NULL;
$arguments20['rel'] = NULL;
$arguments20['rev'] = NULL;
$arguments20['target'] = NULL;
$renderChildrenClosure24 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
return '
								<span aria-hidden="true">&laquo;</span>
							';
};
$viewHelper25 = $self->getViewHelper('$viewHelper25', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper25->setArguments($arguments20);
$viewHelper25->setRenderingContext($renderingContext);
$viewHelper25->setRenderChildrenClosure($renderChildrenClosure24);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output19 .= $viewHelper25->initializeArgumentsAndRender();

$output19 .= '
						';
return $output19;
};

$output6 .= TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper::renderStatic($arguments17, $renderChildrenClosure18, $renderingContext);

$output6 .= '
					';
return $output6;
};
$arguments4['__thenClosure'] = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output26 = '';

$output26 .= '
							';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments27 = array();
// Rendering Array
$array28 = array();
$array28['currentPage'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('pagination'), 'previousPage', $renderingContext);
$arguments27['arguments'] = $array28;
$arguments27['addQueryStringMethod'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'addQueryStringMethod', $renderingContext);
$arguments27['section'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'section', $renderingContext);
// Rendering Array
$array29 = array();
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments30 = array();
$arguments30['key'] = 'widget.pagination.previous';
$arguments30['id'] = NULL;
$arguments30['default'] = NULL;
$arguments30['htmlEscape'] = NULL;
$arguments30['arguments'] = NULL;
$arguments30['extensionName'] = NULL;
$renderChildrenClosure31 = function() {return NULL;};
$array29['aria-label'] = TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper::renderStatic($arguments30, $renderChildrenClosure31, $renderingContext);
$arguments27['additionalAttributes'] = $array29;
$arguments27['data'] = NULL;
$arguments27['action'] = NULL;
$arguments27['format'] = '';
$arguments27['ajax'] = false;
$arguments27['class'] = NULL;
$arguments27['dir'] = NULL;
$arguments27['id'] = NULL;
$arguments27['lang'] = NULL;
$arguments27['style'] = NULL;
$arguments27['title'] = NULL;
$arguments27['accesskey'] = NULL;
$arguments27['tabindex'] = NULL;
$arguments27['onclick'] = NULL;
$arguments27['name'] = NULL;
$arguments27['rel'] = NULL;
$arguments27['rev'] = NULL;
$arguments27['target'] = NULL;
$renderChildrenClosure32 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
return '
								<span aria-hidden="true">&laquo;</span>
							';
};
$viewHelper33 = $self->getViewHelper('$viewHelper33', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper33->setArguments($arguments27);
$viewHelper33->setRenderingContext($renderingContext);
$viewHelper33->setRenderChildrenClosure($renderChildrenClosure32);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output26 .= $viewHelper33->initializeArgumentsAndRender();

$output26 .= '
						';
return $output26;
};
$arguments4['__elseClosure'] = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output34 = '';

$output34 .= '
							';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments35 = array();
$arguments35['addQueryStringMethod'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'addQueryStringMethod', $renderingContext);
$arguments35['section'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'section', $renderingContext);
// Rendering Array
$array36 = array();
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments37 = array();
$arguments37['key'] = 'widget.pagination.previous';
$arguments37['id'] = NULL;
$arguments37['default'] = NULL;
$arguments37['htmlEscape'] = NULL;
$arguments37['arguments'] = NULL;
$arguments37['extensionName'] = NULL;
$renderChildrenClosure38 = function() {return NULL;};
$array36['aria-label'] = TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper::renderStatic($arguments37, $renderChildrenClosure38, $renderingContext);
$arguments35['additionalAttributes'] = $array36;
$arguments35['data'] = NULL;
$arguments35['action'] = NULL;
$arguments35['arguments'] = array (
);
$arguments35['format'] = '';
$arguments35['ajax'] = false;
$arguments35['class'] = NULL;
$arguments35['dir'] = NULL;
$arguments35['id'] = NULL;
$arguments35['lang'] = NULL;
$arguments35['style'] = NULL;
$arguments35['title'] = NULL;
$arguments35['accesskey'] = NULL;
$arguments35['tabindex'] = NULL;
$arguments35['onclick'] = NULL;
$arguments35['name'] = NULL;
$arguments35['rel'] = NULL;
$arguments35['rev'] = NULL;
$arguments35['target'] = NULL;
$renderChildrenClosure39 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
return '
								<span aria-hidden="true">&laquo;</span>
							';
};
$viewHelper40 = $self->getViewHelper('$viewHelper40', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper40->setArguments($arguments35);
$viewHelper40->setRenderingContext($renderingContext);
$viewHelper40->setRenderChildrenClosure($renderChildrenClosure39);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output34 .= $viewHelper40->initializeArgumentsAndRender();

$output34 .= '
						';
return $output34;
};

$output3 .= TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper::renderStatic($arguments4, $renderChildrenClosure5, $renderingContext);

$output3 .= '
				</li>
			';
return $output3;
};

$output0 .= TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper::renderStatic($arguments1, $renderChildrenClosure2, $renderingContext);

$output0 .= '
			';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments41 = array();
// Rendering Boolean node
$arguments41['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('>', \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('pagination'), 'displayRangeStart', $renderingContext), 1);
$arguments41['then'] = NULL;
$arguments41['else'] = NULL;
$renderChildrenClosure42 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output43 = '';

$output43 .= '
				<li class="first">
					';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments44 = array();
$arguments44['addQueryStringMethod'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'addQueryStringMethod', $renderingContext);
$arguments44['section'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'section', $renderingContext);
$arguments44['additionalAttributes'] = NULL;
$arguments44['data'] = NULL;
$arguments44['action'] = NULL;
$arguments44['arguments'] = array (
);
$arguments44['format'] = '';
$arguments44['ajax'] = false;
$arguments44['class'] = NULL;
$arguments44['dir'] = NULL;
$arguments44['id'] = NULL;
$arguments44['lang'] = NULL;
$arguments44['style'] = NULL;
$arguments44['title'] = NULL;
$arguments44['accesskey'] = NULL;
$arguments44['tabindex'] = NULL;
$arguments44['onclick'] = NULL;
$arguments44['name'] = NULL;
$arguments44['rel'] = NULL;
$arguments44['rev'] = NULL;
$arguments44['target'] = NULL;
$renderChildrenClosure45 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
return '1';
};
$viewHelper46 = $self->getViewHelper('$viewHelper46', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper46->setArguments($arguments44);
$viewHelper46->setRenderingContext($renderingContext);
$viewHelper46->setRenderChildrenClosure($renderChildrenClosure45);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output43 .= $viewHelper46->initializeArgumentsAndRender();

$output43 .= '
				</li>
			';
return $output43;
};

$output0 .= TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper::renderStatic($arguments41, $renderChildrenClosure42, $renderingContext);

$output0 .= '
			';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments47 = array();
// Rendering Boolean node
$arguments47['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('pagination'), 'hasLessPages', $renderingContext));
$arguments47['then'] = NULL;
$arguments47['else'] = NULL;
$renderChildrenClosure48 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
return '
				<li>...</li>
			';
};

$output0 .= TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper::renderStatic($arguments47, $renderChildrenClosure48, $renderingContext);

$output0 .= '
			';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ForViewHelper
$arguments49 = array();
$arguments49['each'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('pagination'), 'pages', $renderingContext);
$arguments49['as'] = 'page';
$arguments49['key'] = '';
$arguments49['reverse'] = false;
$arguments49['iteration'] = NULL;
$renderChildrenClosure50 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output51 = '';

$output51 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments52 = array();
// Rendering Boolean node
$arguments52['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'isCurrent', $renderingContext));
$arguments52['then'] = NULL;
$arguments52['else'] = NULL;
$renderChildrenClosure53 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output54 = '';

$output54 .= '
					';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper
$arguments55 = array();
$renderChildrenClosure56 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output57 = '';

$output57 .= '
						<li class="active">
							';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments58 = array();
// Rendering Array
$array59 = array();
$array59['currentPage'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext);
$arguments58['arguments'] = $array59;
$arguments58['addQueryStringMethod'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'addQueryStringMethod', $renderingContext);
$arguments58['section'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'section', $renderingContext);
$arguments58['additionalAttributes'] = NULL;
$arguments58['data'] = NULL;
$arguments58['action'] = NULL;
$arguments58['format'] = '';
$arguments58['ajax'] = false;
$arguments58['class'] = NULL;
$arguments58['dir'] = NULL;
$arguments58['id'] = NULL;
$arguments58['lang'] = NULL;
$arguments58['style'] = NULL;
$arguments58['title'] = NULL;
$arguments58['accesskey'] = NULL;
$arguments58['tabindex'] = NULL;
$arguments58['onclick'] = NULL;
$arguments58['name'] = NULL;
$arguments58['rel'] = NULL;
$arguments58['rev'] = NULL;
$arguments58['target'] = NULL;
$renderChildrenClosure60 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output61 = '';

$output61 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments62 = array();
$arguments62['value'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext);
$arguments62['keepQuotes'] = false;
$arguments62['encoding'] = NULL;
$arguments62['doubleEncode'] = true;
$renderChildrenClosure63 = function() {return NULL;};
$value64 = ($arguments62['value'] !== NULL ? $arguments62['value'] : $renderChildrenClosure63());

$output61 .= (!is_string($value64) ? $value64 : htmlspecialchars($value64, ($arguments62['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments62['encoding'] !== NULL ? $arguments62['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments62['doubleEncode']));

$output61 .= '
							';
return $output61;
};
$viewHelper65 = $self->getViewHelper('$viewHelper65', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper65->setArguments($arguments58);
$viewHelper65->setRenderingContext($renderingContext);
$viewHelper65->setRenderChildrenClosure($renderChildrenClosure60);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output57 .= $viewHelper65->initializeArgumentsAndRender();

$output57 .= '
						</li>
					';
return $output57;
};

$output54 .= TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper::renderStatic($arguments55, $renderChildrenClosure56, $renderingContext);

$output54 .= '
					';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper
$arguments66 = array();
$renderChildrenClosure67 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output68 = '';

$output68 .= '
						<li>
							';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments69 = array();
// Rendering Boolean node
$arguments69['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('>', \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext), 1);
$arguments69['then'] = NULL;
$arguments69['else'] = NULL;
$renderChildrenClosure70 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output71 = '';

$output71 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper
$arguments72 = array();
$renderChildrenClosure73 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output74 = '';

$output74 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments75 = array();
// Rendering Array
$array76 = array();
$array76['currentPage'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext);
$arguments75['arguments'] = $array76;
$arguments75['addQueryStringMethod'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'addQueryStringMethod', $renderingContext);
$arguments75['section'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'section', $renderingContext);
$arguments75['additionalAttributes'] = NULL;
$arguments75['data'] = NULL;
$arguments75['action'] = NULL;
$arguments75['format'] = '';
$arguments75['ajax'] = false;
$arguments75['class'] = NULL;
$arguments75['dir'] = NULL;
$arguments75['id'] = NULL;
$arguments75['lang'] = NULL;
$arguments75['style'] = NULL;
$arguments75['title'] = NULL;
$arguments75['accesskey'] = NULL;
$arguments75['tabindex'] = NULL;
$arguments75['onclick'] = NULL;
$arguments75['name'] = NULL;
$arguments75['rel'] = NULL;
$arguments75['rev'] = NULL;
$arguments75['target'] = NULL;
$renderChildrenClosure77 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output78 = '';

$output78 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments79 = array();
$arguments79['value'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext);
$arguments79['keepQuotes'] = false;
$arguments79['encoding'] = NULL;
$arguments79['doubleEncode'] = true;
$renderChildrenClosure80 = function() {return NULL;};
$value81 = ($arguments79['value'] !== NULL ? $arguments79['value'] : $renderChildrenClosure80());

$output78 .= (!is_string($value81) ? $value81 : htmlspecialchars($value81, ($arguments79['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments79['encoding'] !== NULL ? $arguments79['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments79['doubleEncode']));

$output78 .= '
									';
return $output78;
};
$viewHelper82 = $self->getViewHelper('$viewHelper82', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper82->setArguments($arguments75);
$viewHelper82->setRenderingContext($renderingContext);
$viewHelper82->setRenderChildrenClosure($renderChildrenClosure77);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output74 .= $viewHelper82->initializeArgumentsAndRender();

$output74 .= '
								';
return $output74;
};

$output71 .= TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper::renderStatic($arguments72, $renderChildrenClosure73, $renderingContext);

$output71 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper
$arguments83 = array();
$renderChildrenClosure84 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output85 = '';

$output85 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments86 = array();
$arguments86['addQueryStringMethod'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'addQueryStringMethod', $renderingContext);
$arguments86['section'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'section', $renderingContext);
$arguments86['additionalAttributes'] = NULL;
$arguments86['data'] = NULL;
$arguments86['action'] = NULL;
$arguments86['arguments'] = array (
);
$arguments86['format'] = '';
$arguments86['ajax'] = false;
$arguments86['class'] = NULL;
$arguments86['dir'] = NULL;
$arguments86['id'] = NULL;
$arguments86['lang'] = NULL;
$arguments86['style'] = NULL;
$arguments86['title'] = NULL;
$arguments86['accesskey'] = NULL;
$arguments86['tabindex'] = NULL;
$arguments86['onclick'] = NULL;
$arguments86['name'] = NULL;
$arguments86['rel'] = NULL;
$arguments86['rev'] = NULL;
$arguments86['target'] = NULL;
$renderChildrenClosure87 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output88 = '';

$output88 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments89 = array();
$arguments89['value'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext);
$arguments89['keepQuotes'] = false;
$arguments89['encoding'] = NULL;
$arguments89['doubleEncode'] = true;
$renderChildrenClosure90 = function() {return NULL;};
$value91 = ($arguments89['value'] !== NULL ? $arguments89['value'] : $renderChildrenClosure90());

$output88 .= (!is_string($value91) ? $value91 : htmlspecialchars($value91, ($arguments89['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments89['encoding'] !== NULL ? $arguments89['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments89['doubleEncode']));

$output88 .= '
									';
return $output88;
};
$viewHelper92 = $self->getViewHelper('$viewHelper92', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper92->setArguments($arguments86);
$viewHelper92->setRenderingContext($renderingContext);
$viewHelper92->setRenderChildrenClosure($renderChildrenClosure87);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output85 .= $viewHelper92->initializeArgumentsAndRender();

$output85 .= '
								';
return $output85;
};

$output71 .= TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper::renderStatic($arguments83, $renderChildrenClosure84, $renderingContext);

$output71 .= '
							';
return $output71;
};
$arguments69['__thenClosure'] = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output93 = '';

$output93 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments94 = array();
// Rendering Array
$array95 = array();
$array95['currentPage'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext);
$arguments94['arguments'] = $array95;
$arguments94['addQueryStringMethod'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'addQueryStringMethod', $renderingContext);
$arguments94['section'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'section', $renderingContext);
$arguments94['additionalAttributes'] = NULL;
$arguments94['data'] = NULL;
$arguments94['action'] = NULL;
$arguments94['format'] = '';
$arguments94['ajax'] = false;
$arguments94['class'] = NULL;
$arguments94['dir'] = NULL;
$arguments94['id'] = NULL;
$arguments94['lang'] = NULL;
$arguments94['style'] = NULL;
$arguments94['title'] = NULL;
$arguments94['accesskey'] = NULL;
$arguments94['tabindex'] = NULL;
$arguments94['onclick'] = NULL;
$arguments94['name'] = NULL;
$arguments94['rel'] = NULL;
$arguments94['rev'] = NULL;
$arguments94['target'] = NULL;
$renderChildrenClosure96 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output97 = '';

$output97 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments98 = array();
$arguments98['value'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext);
$arguments98['keepQuotes'] = false;
$arguments98['encoding'] = NULL;
$arguments98['doubleEncode'] = true;
$renderChildrenClosure99 = function() {return NULL;};
$value100 = ($arguments98['value'] !== NULL ? $arguments98['value'] : $renderChildrenClosure99());

$output97 .= (!is_string($value100) ? $value100 : htmlspecialchars($value100, ($arguments98['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments98['encoding'] !== NULL ? $arguments98['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments98['doubleEncode']));

$output97 .= '
									';
return $output97;
};
$viewHelper101 = $self->getViewHelper('$viewHelper101', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper101->setArguments($arguments94);
$viewHelper101->setRenderingContext($renderingContext);
$viewHelper101->setRenderChildrenClosure($renderChildrenClosure96);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output93 .= $viewHelper101->initializeArgumentsAndRender();

$output93 .= '
								';
return $output93;
};
$arguments69['__elseClosure'] = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output102 = '';

$output102 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments103 = array();
$arguments103['addQueryStringMethod'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'addQueryStringMethod', $renderingContext);
$arguments103['section'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'section', $renderingContext);
$arguments103['additionalAttributes'] = NULL;
$arguments103['data'] = NULL;
$arguments103['action'] = NULL;
$arguments103['arguments'] = array (
);
$arguments103['format'] = '';
$arguments103['ajax'] = false;
$arguments103['class'] = NULL;
$arguments103['dir'] = NULL;
$arguments103['id'] = NULL;
$arguments103['lang'] = NULL;
$arguments103['style'] = NULL;
$arguments103['title'] = NULL;
$arguments103['accesskey'] = NULL;
$arguments103['tabindex'] = NULL;
$arguments103['onclick'] = NULL;
$arguments103['name'] = NULL;
$arguments103['rel'] = NULL;
$arguments103['rev'] = NULL;
$arguments103['target'] = NULL;
$renderChildrenClosure104 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output105 = '';

$output105 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments106 = array();
$arguments106['value'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext);
$arguments106['keepQuotes'] = false;
$arguments106['encoding'] = NULL;
$arguments106['doubleEncode'] = true;
$renderChildrenClosure107 = function() {return NULL;};
$value108 = ($arguments106['value'] !== NULL ? $arguments106['value'] : $renderChildrenClosure107());

$output105 .= (!is_string($value108) ? $value108 : htmlspecialchars($value108, ($arguments106['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments106['encoding'] !== NULL ? $arguments106['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments106['doubleEncode']));

$output105 .= '
									';
return $output105;
};
$viewHelper109 = $self->getViewHelper('$viewHelper109', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper109->setArguments($arguments103);
$viewHelper109->setRenderingContext($renderingContext);
$viewHelper109->setRenderChildrenClosure($renderChildrenClosure104);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output102 .= $viewHelper109->initializeArgumentsAndRender();

$output102 .= '
								';
return $output102;
};

$output68 .= TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper::renderStatic($arguments69, $renderChildrenClosure70, $renderingContext);

$output68 .= '
						</li>
					';
return $output68;
};

$output54 .= TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper::renderStatic($arguments66, $renderChildrenClosure67, $renderingContext);

$output54 .= '
				';
return $output54;
};
$arguments52['__thenClosure'] = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output110 = '';

$output110 .= '
						<li class="active">
							';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments111 = array();
// Rendering Array
$array112 = array();
$array112['currentPage'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext);
$arguments111['arguments'] = $array112;
$arguments111['addQueryStringMethod'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'addQueryStringMethod', $renderingContext);
$arguments111['section'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'section', $renderingContext);
$arguments111['additionalAttributes'] = NULL;
$arguments111['data'] = NULL;
$arguments111['action'] = NULL;
$arguments111['format'] = '';
$arguments111['ajax'] = false;
$arguments111['class'] = NULL;
$arguments111['dir'] = NULL;
$arguments111['id'] = NULL;
$arguments111['lang'] = NULL;
$arguments111['style'] = NULL;
$arguments111['title'] = NULL;
$arguments111['accesskey'] = NULL;
$arguments111['tabindex'] = NULL;
$arguments111['onclick'] = NULL;
$arguments111['name'] = NULL;
$arguments111['rel'] = NULL;
$arguments111['rev'] = NULL;
$arguments111['target'] = NULL;
$renderChildrenClosure113 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output114 = '';

$output114 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments115 = array();
$arguments115['value'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext);
$arguments115['keepQuotes'] = false;
$arguments115['encoding'] = NULL;
$arguments115['doubleEncode'] = true;
$renderChildrenClosure116 = function() {return NULL;};
$value117 = ($arguments115['value'] !== NULL ? $arguments115['value'] : $renderChildrenClosure116());

$output114 .= (!is_string($value117) ? $value117 : htmlspecialchars($value117, ($arguments115['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments115['encoding'] !== NULL ? $arguments115['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments115['doubleEncode']));

$output114 .= '
							';
return $output114;
};
$viewHelper118 = $self->getViewHelper('$viewHelper118', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper118->setArguments($arguments111);
$viewHelper118->setRenderingContext($renderingContext);
$viewHelper118->setRenderChildrenClosure($renderChildrenClosure113);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output110 .= $viewHelper118->initializeArgumentsAndRender();

$output110 .= '
						</li>
					';
return $output110;
};
$arguments52['__elseClosure'] = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output119 = '';

$output119 .= '
						<li>
							';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments120 = array();
// Rendering Boolean node
$arguments120['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('>', \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext), 1);
$arguments120['then'] = NULL;
$arguments120['else'] = NULL;
$renderChildrenClosure121 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output122 = '';

$output122 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper
$arguments123 = array();
$renderChildrenClosure124 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output125 = '';

$output125 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments126 = array();
// Rendering Array
$array127 = array();
$array127['currentPage'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext);
$arguments126['arguments'] = $array127;
$arguments126['addQueryStringMethod'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'addQueryStringMethod', $renderingContext);
$arguments126['section'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'section', $renderingContext);
$arguments126['additionalAttributes'] = NULL;
$arguments126['data'] = NULL;
$arguments126['action'] = NULL;
$arguments126['format'] = '';
$arguments126['ajax'] = false;
$arguments126['class'] = NULL;
$arguments126['dir'] = NULL;
$arguments126['id'] = NULL;
$arguments126['lang'] = NULL;
$arguments126['style'] = NULL;
$arguments126['title'] = NULL;
$arguments126['accesskey'] = NULL;
$arguments126['tabindex'] = NULL;
$arguments126['onclick'] = NULL;
$arguments126['name'] = NULL;
$arguments126['rel'] = NULL;
$arguments126['rev'] = NULL;
$arguments126['target'] = NULL;
$renderChildrenClosure128 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output129 = '';

$output129 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments130 = array();
$arguments130['value'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext);
$arguments130['keepQuotes'] = false;
$arguments130['encoding'] = NULL;
$arguments130['doubleEncode'] = true;
$renderChildrenClosure131 = function() {return NULL;};
$value132 = ($arguments130['value'] !== NULL ? $arguments130['value'] : $renderChildrenClosure131());

$output129 .= (!is_string($value132) ? $value132 : htmlspecialchars($value132, ($arguments130['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments130['encoding'] !== NULL ? $arguments130['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments130['doubleEncode']));

$output129 .= '
									';
return $output129;
};
$viewHelper133 = $self->getViewHelper('$viewHelper133', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper133->setArguments($arguments126);
$viewHelper133->setRenderingContext($renderingContext);
$viewHelper133->setRenderChildrenClosure($renderChildrenClosure128);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output125 .= $viewHelper133->initializeArgumentsAndRender();

$output125 .= '
								';
return $output125;
};

$output122 .= TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper::renderStatic($arguments123, $renderChildrenClosure124, $renderingContext);

$output122 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper
$arguments134 = array();
$renderChildrenClosure135 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output136 = '';

$output136 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments137 = array();
$arguments137['addQueryStringMethod'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'addQueryStringMethod', $renderingContext);
$arguments137['section'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'section', $renderingContext);
$arguments137['additionalAttributes'] = NULL;
$arguments137['data'] = NULL;
$arguments137['action'] = NULL;
$arguments137['arguments'] = array (
);
$arguments137['format'] = '';
$arguments137['ajax'] = false;
$arguments137['class'] = NULL;
$arguments137['dir'] = NULL;
$arguments137['id'] = NULL;
$arguments137['lang'] = NULL;
$arguments137['style'] = NULL;
$arguments137['title'] = NULL;
$arguments137['accesskey'] = NULL;
$arguments137['tabindex'] = NULL;
$arguments137['onclick'] = NULL;
$arguments137['name'] = NULL;
$arguments137['rel'] = NULL;
$arguments137['rev'] = NULL;
$arguments137['target'] = NULL;
$renderChildrenClosure138 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output139 = '';

$output139 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments140 = array();
$arguments140['value'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext);
$arguments140['keepQuotes'] = false;
$arguments140['encoding'] = NULL;
$arguments140['doubleEncode'] = true;
$renderChildrenClosure141 = function() {return NULL;};
$value142 = ($arguments140['value'] !== NULL ? $arguments140['value'] : $renderChildrenClosure141());

$output139 .= (!is_string($value142) ? $value142 : htmlspecialchars($value142, ($arguments140['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments140['encoding'] !== NULL ? $arguments140['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments140['doubleEncode']));

$output139 .= '
									';
return $output139;
};
$viewHelper143 = $self->getViewHelper('$viewHelper143', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper143->setArguments($arguments137);
$viewHelper143->setRenderingContext($renderingContext);
$viewHelper143->setRenderChildrenClosure($renderChildrenClosure138);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output136 .= $viewHelper143->initializeArgumentsAndRender();

$output136 .= '
								';
return $output136;
};

$output122 .= TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper::renderStatic($arguments134, $renderChildrenClosure135, $renderingContext);

$output122 .= '
							';
return $output122;
};
$arguments120['__thenClosure'] = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output144 = '';

$output144 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments145 = array();
// Rendering Array
$array146 = array();
$array146['currentPage'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext);
$arguments145['arguments'] = $array146;
$arguments145['addQueryStringMethod'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'addQueryStringMethod', $renderingContext);
$arguments145['section'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'section', $renderingContext);
$arguments145['additionalAttributes'] = NULL;
$arguments145['data'] = NULL;
$arguments145['action'] = NULL;
$arguments145['format'] = '';
$arguments145['ajax'] = false;
$arguments145['class'] = NULL;
$arguments145['dir'] = NULL;
$arguments145['id'] = NULL;
$arguments145['lang'] = NULL;
$arguments145['style'] = NULL;
$arguments145['title'] = NULL;
$arguments145['accesskey'] = NULL;
$arguments145['tabindex'] = NULL;
$arguments145['onclick'] = NULL;
$arguments145['name'] = NULL;
$arguments145['rel'] = NULL;
$arguments145['rev'] = NULL;
$arguments145['target'] = NULL;
$renderChildrenClosure147 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output148 = '';

$output148 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments149 = array();
$arguments149['value'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext);
$arguments149['keepQuotes'] = false;
$arguments149['encoding'] = NULL;
$arguments149['doubleEncode'] = true;
$renderChildrenClosure150 = function() {return NULL;};
$value151 = ($arguments149['value'] !== NULL ? $arguments149['value'] : $renderChildrenClosure150());

$output148 .= (!is_string($value151) ? $value151 : htmlspecialchars($value151, ($arguments149['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments149['encoding'] !== NULL ? $arguments149['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments149['doubleEncode']));

$output148 .= '
									';
return $output148;
};
$viewHelper152 = $self->getViewHelper('$viewHelper152', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper152->setArguments($arguments145);
$viewHelper152->setRenderingContext($renderingContext);
$viewHelper152->setRenderChildrenClosure($renderChildrenClosure147);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output144 .= $viewHelper152->initializeArgumentsAndRender();

$output144 .= '
								';
return $output144;
};
$arguments120['__elseClosure'] = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output153 = '';

$output153 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments154 = array();
$arguments154['addQueryStringMethod'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'addQueryStringMethod', $renderingContext);
$arguments154['section'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'section', $renderingContext);
$arguments154['additionalAttributes'] = NULL;
$arguments154['data'] = NULL;
$arguments154['action'] = NULL;
$arguments154['arguments'] = array (
);
$arguments154['format'] = '';
$arguments154['ajax'] = false;
$arguments154['class'] = NULL;
$arguments154['dir'] = NULL;
$arguments154['id'] = NULL;
$arguments154['lang'] = NULL;
$arguments154['style'] = NULL;
$arguments154['title'] = NULL;
$arguments154['accesskey'] = NULL;
$arguments154['tabindex'] = NULL;
$arguments154['onclick'] = NULL;
$arguments154['name'] = NULL;
$arguments154['rel'] = NULL;
$arguments154['rev'] = NULL;
$arguments154['target'] = NULL;
$renderChildrenClosure155 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output156 = '';

$output156 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments157 = array();
$arguments157['value'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext);
$arguments157['keepQuotes'] = false;
$arguments157['encoding'] = NULL;
$arguments157['doubleEncode'] = true;
$renderChildrenClosure158 = function() {return NULL;};
$value159 = ($arguments157['value'] !== NULL ? $arguments157['value'] : $renderChildrenClosure158());

$output156 .= (!is_string($value159) ? $value159 : htmlspecialchars($value159, ($arguments157['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments157['encoding'] !== NULL ? $arguments157['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments157['doubleEncode']));

$output156 .= '
									';
return $output156;
};
$viewHelper160 = $self->getViewHelper('$viewHelper160', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper160->setArguments($arguments154);
$viewHelper160->setRenderingContext($renderingContext);
$viewHelper160->setRenderChildrenClosure($renderChildrenClosure155);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output153 .= $viewHelper160->initializeArgumentsAndRender();

$output153 .= '
								';
return $output153;
};

$output119 .= TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper::renderStatic($arguments120, $renderChildrenClosure121, $renderingContext);

$output119 .= '
						</li>
					';
return $output119;
};

$output51 .= TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper::renderStatic($arguments52, $renderChildrenClosure53, $renderingContext);

$output51 .= '
			';
return $output51;
};

$output0 .= TYPO3\CMS\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments49, $renderChildrenClosure50, $renderingContext);

$output0 .= '
			';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments161 = array();
// Rendering Boolean node
$arguments161['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('pagination'), 'hasMorePages', $renderingContext));
$arguments161['then'] = NULL;
$arguments161['else'] = NULL;
$renderChildrenClosure162 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
return '
				<li>...</li>
			';
};

$output0 .= TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper::renderStatic($arguments161, $renderChildrenClosure162, $renderingContext);

$output0 .= '
			';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments163 = array();
// Rendering Boolean node
$arguments163['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('<', \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('pagination'), 'displayRangeEnd', $renderingContext), \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('pagination'), 'numberOfPages', $renderingContext));
$arguments163['then'] = NULL;
$arguments163['else'] = NULL;
$renderChildrenClosure164 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output165 = '';

$output165 .= '
				<li class="last">
					';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments166 = array();
// Rendering Array
$array167 = array();
$array167['currentPage'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('pagination'), 'numberOfPages', $renderingContext);
$arguments166['arguments'] = $array167;
$arguments166['addQueryStringMethod'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'addQueryStringMethod', $renderingContext);
$arguments166['section'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'section', $renderingContext);
$arguments166['additionalAttributes'] = NULL;
$arguments166['data'] = NULL;
$arguments166['action'] = NULL;
$arguments166['format'] = '';
$arguments166['ajax'] = false;
$arguments166['class'] = NULL;
$arguments166['dir'] = NULL;
$arguments166['id'] = NULL;
$arguments166['lang'] = NULL;
$arguments166['style'] = NULL;
$arguments166['title'] = NULL;
$arguments166['accesskey'] = NULL;
$arguments166['tabindex'] = NULL;
$arguments166['onclick'] = NULL;
$arguments166['name'] = NULL;
$arguments166['rel'] = NULL;
$arguments166['rev'] = NULL;
$arguments166['target'] = NULL;
$renderChildrenClosure168 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output169 = '';

$output169 .= '
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments170 = array();
$arguments170['value'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('pagination'), 'numberOfPages', $renderingContext);
$arguments170['keepQuotes'] = false;
$arguments170['encoding'] = NULL;
$arguments170['doubleEncode'] = true;
$renderChildrenClosure171 = function() {return NULL;};
$value172 = ($arguments170['value'] !== NULL ? $arguments170['value'] : $renderChildrenClosure171());

$output169 .= (!is_string($value172) ? $value172 : htmlspecialchars($value172, ($arguments170['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments170['encoding'] !== NULL ? $arguments170['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments170['doubleEncode']));

$output169 .= '
					';
return $output169;
};
$viewHelper173 = $self->getViewHelper('$viewHelper173', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper173->setArguments($arguments166);
$viewHelper173->setRenderingContext($renderingContext);
$viewHelper173->setRenderChildrenClosure($renderChildrenClosure168);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output165 .= $viewHelper173->initializeArgumentsAndRender();

$output165 .= '
				</li>
			';
return $output165;
};

$output0 .= TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper::renderStatic($arguments163, $renderChildrenClosure164, $renderingContext);

$output0 .= '
			';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments174 = array();
// Rendering Boolean node
$arguments174['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('pagination'), 'nextPage', $renderingContext));
$arguments174['then'] = NULL;
$arguments174['else'] = NULL;
$renderChildrenClosure175 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output176 = '';

$output176 .= '
				<li class="next">
					';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments177 = array();
// Rendering Array
$array178 = array();
$array178['currentPage'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('pagination'), 'nextPage', $renderingContext);
$arguments177['arguments'] = $array178;
$arguments177['addQueryStringMethod'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'addQueryStringMethod', $renderingContext);
$arguments177['section'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'section', $renderingContext);
// Rendering Array
$array179 = array();
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments180 = array();
$arguments180['key'] = 'widget.pagination.next';
$arguments180['id'] = NULL;
$arguments180['default'] = NULL;
$arguments180['htmlEscape'] = NULL;
$arguments180['arguments'] = NULL;
$arguments180['extensionName'] = NULL;
$renderChildrenClosure181 = function() {return NULL;};
$array179['aria-label'] = TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper::renderStatic($arguments180, $renderChildrenClosure181, $renderingContext);
$arguments177['additionalAttributes'] = $array179;
$arguments177['data'] = NULL;
$arguments177['action'] = NULL;
$arguments177['format'] = '';
$arguments177['ajax'] = false;
$arguments177['class'] = NULL;
$arguments177['dir'] = NULL;
$arguments177['id'] = NULL;
$arguments177['lang'] = NULL;
$arguments177['style'] = NULL;
$arguments177['title'] = NULL;
$arguments177['accesskey'] = NULL;
$arguments177['tabindex'] = NULL;
$arguments177['onclick'] = NULL;
$arguments177['name'] = NULL;
$arguments177['rel'] = NULL;
$arguments177['rev'] = NULL;
$arguments177['target'] = NULL;
$renderChildrenClosure182 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
return '
						<span aria-hidden="true">&raquo;</span>
					';
};
$viewHelper183 = $self->getViewHelper('$viewHelper183', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper183->setArguments($arguments177);
$viewHelper183->setRenderingContext($renderingContext);
$viewHelper183->setRenderChildrenClosure($renderChildrenClosure182);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output176 .= $viewHelper183->initializeArgumentsAndRender();

$output176 .= '
				</li>
			';
return $output176;
};

$output0 .= TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper::renderStatic($arguments174, $renderChildrenClosure175, $renderingContext);

$output0 .= '
		</ul>
	</nav>
';


return $output0;
}
/**
 * section title
 */
public function section_3c6de1b7dd91465d437ef415f94f36afc1fbc8a8(\TYPO3\CMS\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();

$output184 = '';

$output184 .= '
	';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ForViewHelper
$arguments185 = array();
$arguments185['each'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('pagination'), 'pages', $renderingContext);
$arguments185['as'] = 'page';
$arguments185['key'] = '';
$arguments185['reverse'] = false;
$arguments185['iteration'] = NULL;
$renderChildrenClosure186 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output187 = '';

$output187 .= '
		';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments188 = array();
// Rendering Boolean node
$arguments188['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'isCurrent', $renderingContext));
$arguments188['then'] = NULL;
$arguments188['else'] = NULL;
$renderChildrenClosure189 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output190 = '';

$output190 .= '
			';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments191 = array();
// Rendering Boolean node
$arguments191['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('>', \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext), 1);
$arguments191['then'] = NULL;
$arguments191['else'] = NULL;
$renderChildrenClosure192 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output193 = '';

$output193 .= '
				';
// Rendering ViewHelper TYPO3\T3extblog\ViewHelpers\Frontend\TitleTagViewHelper
$arguments194 = array();
$arguments194['prepend'] = true;
$arguments194['searchTitle'] = NULL;
$renderChildrenClosure195 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output196 = '';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments197 = array();
$arguments197['key'] = 'widget.pagination.page';
$arguments197['id'] = NULL;
$arguments197['default'] = NULL;
$arguments197['htmlEscape'] = NULL;
$arguments197['arguments'] = NULL;
$arguments197['extensionName'] = NULL;
$renderChildrenClosure198 = function() {return NULL;};

$output196 .= TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper::renderStatic($arguments197, $renderChildrenClosure198, $renderingContext);

$output196 .= ' ';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments199 = array();
$arguments199['value'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext);
$arguments199['keepQuotes'] = false;
$arguments199['encoding'] = NULL;
$arguments199['doubleEncode'] = true;
$renderChildrenClosure200 = function() {return NULL;};
$value201 = ($arguments199['value'] !== NULL ? $arguments199['value'] : $renderChildrenClosure200());

$output196 .= (!is_string($value201) ? $value201 : htmlspecialchars($value201, ($arguments199['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments199['encoding'] !== NULL ? $arguments199['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments199['doubleEncode']));

$output196 .= ' / ';
return $output196;
};
$viewHelper202 = $self->getViewHelper('$viewHelper202', $renderingContext, 'TYPO3\T3extblog\ViewHelpers\Frontend\TitleTagViewHelper');
$viewHelper202->setArguments($arguments194);
$viewHelper202->setRenderingContext($renderingContext);
$viewHelper202->setRenderChildrenClosure($renderChildrenClosure195);
// End of ViewHelper TYPO3\T3extblog\ViewHelpers\Frontend\TitleTagViewHelper

$output193 .= $viewHelper202->initializeArgumentsAndRender();

$output193 .= '
			';
return $output193;
};

$output190 .= TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper::renderStatic($arguments191, $renderChildrenClosure192, $renderingContext);

$output190 .= '
		';
return $output190;
};

$output187 .= TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper::renderStatic($arguments188, $renderChildrenClosure189, $renderingContext);

$output187 .= '
	';
return $output187;
};

$output184 .= TYPO3\CMS\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments185, $renderChildrenClosure186, $renderingContext);

$output184 .= '
';


return $output184;
}
/**
 * Main Render function
 */
public function render(\TYPO3\CMS\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();

$output203 = '';

$output203 .= '

';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper
$arguments204 = array();
$arguments204['section'] = 'title';
// Rendering Array
$array205 = array();
$array205['pagination'] = $currentVariableContainer->getOrNull('pagination');
$arguments204['arguments'] = $array205;
$arguments204['partial'] = NULL;
$arguments204['optional'] = false;
$renderChildrenClosure206 = function() {return NULL;};

$output203 .= TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper::renderStatic($arguments204, $renderChildrenClosure206, $renderingContext);

$output203 .= '

';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments207 = array();
// Rendering Boolean node
$arguments207['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'insertAbove', $renderingContext));
$arguments207['then'] = NULL;
$arguments207['else'] = NULL;
$renderChildrenClosure208 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output209 = '';

$output209 .= '
	';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper
$arguments210 = array();
$arguments210['section'] = 'paginator';
// Rendering Array
$array211 = array();
$array211['pagination'] = $currentVariableContainer->getOrNull('pagination');
$array211['configuration'] = $currentVariableContainer->getOrNull('configuration');
$arguments210['arguments'] = $array211;
$arguments210['partial'] = NULL;
$arguments210['optional'] = false;
$renderChildrenClosure212 = function() {return NULL;};

$output209 .= TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper::renderStatic($arguments210, $renderChildrenClosure212, $renderingContext);

$output209 .= '
';
return $output209;
};

$output203 .= TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper::renderStatic($arguments207, $renderChildrenClosure208, $renderingContext);

$output203 .= '

';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\RenderChildrenViewHelper
$arguments213 = array();
$arguments213['arguments'] = $currentVariableContainer->getOrNull('contentArguments');
$renderChildrenClosure214 = function() {return NULL;};
$viewHelper215 = $self->getViewHelper('$viewHelper215', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\RenderChildrenViewHelper');
$viewHelper215->setArguments($arguments213);
$viewHelper215->setRenderingContext($renderingContext);
$viewHelper215->setRenderChildrenClosure($renderChildrenClosure214);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\RenderChildrenViewHelper

$output203 .= $viewHelper215->initializeArgumentsAndRender();

$output203 .= '

';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments216 = array();
// Rendering Boolean node
$arguments216['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'insertBelow', $renderingContext));
$arguments216['then'] = NULL;
$arguments216['else'] = NULL;
$renderChildrenClosure217 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output218 = '';

$output218 .= '
	';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper
$arguments219 = array();
$arguments219['section'] = 'paginator';
// Rendering Array
$array220 = array();
$array220['pagination'] = $currentVariableContainer->getOrNull('pagination');
$array220['configuration'] = $currentVariableContainer->getOrNull('configuration');
$arguments219['arguments'] = $array220;
$arguments219['partial'] = NULL;
$arguments219['optional'] = false;
$renderChildrenClosure221 = function() {return NULL;};

$output218 .= TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper::renderStatic($arguments219, $renderChildrenClosure221, $renderingContext);

$output218 .= '
';
return $output218;
};

$output203 .= TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper::renderStatic($arguments216, $renderChildrenClosure217, $renderingContext);

$output203 .= '


';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\SectionViewHelper
$arguments222 = array();
$arguments222['name'] = 'paginator';
$renderChildrenClosure223 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output224 = '';

$output224 .= '
	<nav>
		<ul class="pagination">
			';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments225 = array();
// Rendering Boolean node
$arguments225['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('pagination'), 'previousPage', $renderingContext));
$arguments225['then'] = NULL;
$arguments225['else'] = NULL;
$renderChildrenClosure226 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output227 = '';

$output227 .= '
				<li class="previous">
					';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments228 = array();
// Rendering Boolean node
$arguments228['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('>', \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('pagination'), 'previousPage', $renderingContext), 1);
$arguments228['then'] = NULL;
$arguments228['else'] = NULL;
$renderChildrenClosure229 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output230 = '';

$output230 .= '
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper
$arguments231 = array();
$renderChildrenClosure232 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output233 = '';

$output233 .= '
							';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments234 = array();
// Rendering Array
$array235 = array();
$array235['currentPage'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('pagination'), 'previousPage', $renderingContext);
$arguments234['arguments'] = $array235;
$arguments234['addQueryStringMethod'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'addQueryStringMethod', $renderingContext);
$arguments234['section'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'section', $renderingContext);
// Rendering Array
$array236 = array();
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments237 = array();
$arguments237['key'] = 'widget.pagination.previous';
$arguments237['id'] = NULL;
$arguments237['default'] = NULL;
$arguments237['htmlEscape'] = NULL;
$arguments237['arguments'] = NULL;
$arguments237['extensionName'] = NULL;
$renderChildrenClosure238 = function() {return NULL;};
$array236['aria-label'] = TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper::renderStatic($arguments237, $renderChildrenClosure238, $renderingContext);
$arguments234['additionalAttributes'] = $array236;
$arguments234['data'] = NULL;
$arguments234['action'] = NULL;
$arguments234['format'] = '';
$arguments234['ajax'] = false;
$arguments234['class'] = NULL;
$arguments234['dir'] = NULL;
$arguments234['id'] = NULL;
$arguments234['lang'] = NULL;
$arguments234['style'] = NULL;
$arguments234['title'] = NULL;
$arguments234['accesskey'] = NULL;
$arguments234['tabindex'] = NULL;
$arguments234['onclick'] = NULL;
$arguments234['name'] = NULL;
$arguments234['rel'] = NULL;
$arguments234['rev'] = NULL;
$arguments234['target'] = NULL;
$renderChildrenClosure239 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
return '
								<span aria-hidden="true">&laquo;</span>
							';
};
$viewHelper240 = $self->getViewHelper('$viewHelper240', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper240->setArguments($arguments234);
$viewHelper240->setRenderingContext($renderingContext);
$viewHelper240->setRenderChildrenClosure($renderChildrenClosure239);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output233 .= $viewHelper240->initializeArgumentsAndRender();

$output233 .= '
						';
return $output233;
};

$output230 .= TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper::renderStatic($arguments231, $renderChildrenClosure232, $renderingContext);

$output230 .= '
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper
$arguments241 = array();
$renderChildrenClosure242 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output243 = '';

$output243 .= '
							';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments244 = array();
$arguments244['addQueryStringMethod'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'addQueryStringMethod', $renderingContext);
$arguments244['section'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'section', $renderingContext);
// Rendering Array
$array245 = array();
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments246 = array();
$arguments246['key'] = 'widget.pagination.previous';
$arguments246['id'] = NULL;
$arguments246['default'] = NULL;
$arguments246['htmlEscape'] = NULL;
$arguments246['arguments'] = NULL;
$arguments246['extensionName'] = NULL;
$renderChildrenClosure247 = function() {return NULL;};
$array245['aria-label'] = TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper::renderStatic($arguments246, $renderChildrenClosure247, $renderingContext);
$arguments244['additionalAttributes'] = $array245;
$arguments244['data'] = NULL;
$arguments244['action'] = NULL;
$arguments244['arguments'] = array (
);
$arguments244['format'] = '';
$arguments244['ajax'] = false;
$arguments244['class'] = NULL;
$arguments244['dir'] = NULL;
$arguments244['id'] = NULL;
$arguments244['lang'] = NULL;
$arguments244['style'] = NULL;
$arguments244['title'] = NULL;
$arguments244['accesskey'] = NULL;
$arguments244['tabindex'] = NULL;
$arguments244['onclick'] = NULL;
$arguments244['name'] = NULL;
$arguments244['rel'] = NULL;
$arguments244['rev'] = NULL;
$arguments244['target'] = NULL;
$renderChildrenClosure248 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
return '
								<span aria-hidden="true">&laquo;</span>
							';
};
$viewHelper249 = $self->getViewHelper('$viewHelper249', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper249->setArguments($arguments244);
$viewHelper249->setRenderingContext($renderingContext);
$viewHelper249->setRenderChildrenClosure($renderChildrenClosure248);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output243 .= $viewHelper249->initializeArgumentsAndRender();

$output243 .= '
						';
return $output243;
};

$output230 .= TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper::renderStatic($arguments241, $renderChildrenClosure242, $renderingContext);

$output230 .= '
					';
return $output230;
};
$arguments228['__thenClosure'] = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output250 = '';

$output250 .= '
							';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments251 = array();
// Rendering Array
$array252 = array();
$array252['currentPage'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('pagination'), 'previousPage', $renderingContext);
$arguments251['arguments'] = $array252;
$arguments251['addQueryStringMethod'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'addQueryStringMethod', $renderingContext);
$arguments251['section'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'section', $renderingContext);
// Rendering Array
$array253 = array();
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments254 = array();
$arguments254['key'] = 'widget.pagination.previous';
$arguments254['id'] = NULL;
$arguments254['default'] = NULL;
$arguments254['htmlEscape'] = NULL;
$arguments254['arguments'] = NULL;
$arguments254['extensionName'] = NULL;
$renderChildrenClosure255 = function() {return NULL;};
$array253['aria-label'] = TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper::renderStatic($arguments254, $renderChildrenClosure255, $renderingContext);
$arguments251['additionalAttributes'] = $array253;
$arguments251['data'] = NULL;
$arguments251['action'] = NULL;
$arguments251['format'] = '';
$arguments251['ajax'] = false;
$arguments251['class'] = NULL;
$arguments251['dir'] = NULL;
$arguments251['id'] = NULL;
$arguments251['lang'] = NULL;
$arguments251['style'] = NULL;
$arguments251['title'] = NULL;
$arguments251['accesskey'] = NULL;
$arguments251['tabindex'] = NULL;
$arguments251['onclick'] = NULL;
$arguments251['name'] = NULL;
$arguments251['rel'] = NULL;
$arguments251['rev'] = NULL;
$arguments251['target'] = NULL;
$renderChildrenClosure256 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
return '
								<span aria-hidden="true">&laquo;</span>
							';
};
$viewHelper257 = $self->getViewHelper('$viewHelper257', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper257->setArguments($arguments251);
$viewHelper257->setRenderingContext($renderingContext);
$viewHelper257->setRenderChildrenClosure($renderChildrenClosure256);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output250 .= $viewHelper257->initializeArgumentsAndRender();

$output250 .= '
						';
return $output250;
};
$arguments228['__elseClosure'] = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output258 = '';

$output258 .= '
							';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments259 = array();
$arguments259['addQueryStringMethod'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'addQueryStringMethod', $renderingContext);
$arguments259['section'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'section', $renderingContext);
// Rendering Array
$array260 = array();
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments261 = array();
$arguments261['key'] = 'widget.pagination.previous';
$arguments261['id'] = NULL;
$arguments261['default'] = NULL;
$arguments261['htmlEscape'] = NULL;
$arguments261['arguments'] = NULL;
$arguments261['extensionName'] = NULL;
$renderChildrenClosure262 = function() {return NULL;};
$array260['aria-label'] = TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper::renderStatic($arguments261, $renderChildrenClosure262, $renderingContext);
$arguments259['additionalAttributes'] = $array260;
$arguments259['data'] = NULL;
$arguments259['action'] = NULL;
$arguments259['arguments'] = array (
);
$arguments259['format'] = '';
$arguments259['ajax'] = false;
$arguments259['class'] = NULL;
$arguments259['dir'] = NULL;
$arguments259['id'] = NULL;
$arguments259['lang'] = NULL;
$arguments259['style'] = NULL;
$arguments259['title'] = NULL;
$arguments259['accesskey'] = NULL;
$arguments259['tabindex'] = NULL;
$arguments259['onclick'] = NULL;
$arguments259['name'] = NULL;
$arguments259['rel'] = NULL;
$arguments259['rev'] = NULL;
$arguments259['target'] = NULL;
$renderChildrenClosure263 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
return '
								<span aria-hidden="true">&laquo;</span>
							';
};
$viewHelper264 = $self->getViewHelper('$viewHelper264', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper264->setArguments($arguments259);
$viewHelper264->setRenderingContext($renderingContext);
$viewHelper264->setRenderChildrenClosure($renderChildrenClosure263);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output258 .= $viewHelper264->initializeArgumentsAndRender();

$output258 .= '
						';
return $output258;
};

$output227 .= TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper::renderStatic($arguments228, $renderChildrenClosure229, $renderingContext);

$output227 .= '
				</li>
			';
return $output227;
};

$output224 .= TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper::renderStatic($arguments225, $renderChildrenClosure226, $renderingContext);

$output224 .= '
			';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments265 = array();
// Rendering Boolean node
$arguments265['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('>', \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('pagination'), 'displayRangeStart', $renderingContext), 1);
$arguments265['then'] = NULL;
$arguments265['else'] = NULL;
$renderChildrenClosure266 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output267 = '';

$output267 .= '
				<li class="first">
					';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments268 = array();
$arguments268['addQueryStringMethod'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'addQueryStringMethod', $renderingContext);
$arguments268['section'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'section', $renderingContext);
$arguments268['additionalAttributes'] = NULL;
$arguments268['data'] = NULL;
$arguments268['action'] = NULL;
$arguments268['arguments'] = array (
);
$arguments268['format'] = '';
$arguments268['ajax'] = false;
$arguments268['class'] = NULL;
$arguments268['dir'] = NULL;
$arguments268['id'] = NULL;
$arguments268['lang'] = NULL;
$arguments268['style'] = NULL;
$arguments268['title'] = NULL;
$arguments268['accesskey'] = NULL;
$arguments268['tabindex'] = NULL;
$arguments268['onclick'] = NULL;
$arguments268['name'] = NULL;
$arguments268['rel'] = NULL;
$arguments268['rev'] = NULL;
$arguments268['target'] = NULL;
$renderChildrenClosure269 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
return '1';
};
$viewHelper270 = $self->getViewHelper('$viewHelper270', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper270->setArguments($arguments268);
$viewHelper270->setRenderingContext($renderingContext);
$viewHelper270->setRenderChildrenClosure($renderChildrenClosure269);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output267 .= $viewHelper270->initializeArgumentsAndRender();

$output267 .= '
				</li>
			';
return $output267;
};

$output224 .= TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper::renderStatic($arguments265, $renderChildrenClosure266, $renderingContext);

$output224 .= '
			';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments271 = array();
// Rendering Boolean node
$arguments271['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('pagination'), 'hasLessPages', $renderingContext));
$arguments271['then'] = NULL;
$arguments271['else'] = NULL;
$renderChildrenClosure272 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
return '
				<li>...</li>
			';
};

$output224 .= TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper::renderStatic($arguments271, $renderChildrenClosure272, $renderingContext);

$output224 .= '
			';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ForViewHelper
$arguments273 = array();
$arguments273['each'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('pagination'), 'pages', $renderingContext);
$arguments273['as'] = 'page';
$arguments273['key'] = '';
$arguments273['reverse'] = false;
$arguments273['iteration'] = NULL;
$renderChildrenClosure274 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output275 = '';

$output275 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments276 = array();
// Rendering Boolean node
$arguments276['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'isCurrent', $renderingContext));
$arguments276['then'] = NULL;
$arguments276['else'] = NULL;
$renderChildrenClosure277 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output278 = '';

$output278 .= '
					';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper
$arguments279 = array();
$renderChildrenClosure280 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output281 = '';

$output281 .= '
						<li class="active">
							';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments282 = array();
// Rendering Array
$array283 = array();
$array283['currentPage'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext);
$arguments282['arguments'] = $array283;
$arguments282['addQueryStringMethod'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'addQueryStringMethod', $renderingContext);
$arguments282['section'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'section', $renderingContext);
$arguments282['additionalAttributes'] = NULL;
$arguments282['data'] = NULL;
$arguments282['action'] = NULL;
$arguments282['format'] = '';
$arguments282['ajax'] = false;
$arguments282['class'] = NULL;
$arguments282['dir'] = NULL;
$arguments282['id'] = NULL;
$arguments282['lang'] = NULL;
$arguments282['style'] = NULL;
$arguments282['title'] = NULL;
$arguments282['accesskey'] = NULL;
$arguments282['tabindex'] = NULL;
$arguments282['onclick'] = NULL;
$arguments282['name'] = NULL;
$arguments282['rel'] = NULL;
$arguments282['rev'] = NULL;
$arguments282['target'] = NULL;
$renderChildrenClosure284 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output285 = '';

$output285 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments286 = array();
$arguments286['value'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext);
$arguments286['keepQuotes'] = false;
$arguments286['encoding'] = NULL;
$arguments286['doubleEncode'] = true;
$renderChildrenClosure287 = function() {return NULL;};
$value288 = ($arguments286['value'] !== NULL ? $arguments286['value'] : $renderChildrenClosure287());

$output285 .= (!is_string($value288) ? $value288 : htmlspecialchars($value288, ($arguments286['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments286['encoding'] !== NULL ? $arguments286['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments286['doubleEncode']));

$output285 .= '
							';
return $output285;
};
$viewHelper289 = $self->getViewHelper('$viewHelper289', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper289->setArguments($arguments282);
$viewHelper289->setRenderingContext($renderingContext);
$viewHelper289->setRenderChildrenClosure($renderChildrenClosure284);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output281 .= $viewHelper289->initializeArgumentsAndRender();

$output281 .= '
						</li>
					';
return $output281;
};

$output278 .= TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper::renderStatic($arguments279, $renderChildrenClosure280, $renderingContext);

$output278 .= '
					';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper
$arguments290 = array();
$renderChildrenClosure291 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output292 = '';

$output292 .= '
						<li>
							';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments293 = array();
// Rendering Boolean node
$arguments293['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('>', \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext), 1);
$arguments293['then'] = NULL;
$arguments293['else'] = NULL;
$renderChildrenClosure294 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output295 = '';

$output295 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper
$arguments296 = array();
$renderChildrenClosure297 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output298 = '';

$output298 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments299 = array();
// Rendering Array
$array300 = array();
$array300['currentPage'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext);
$arguments299['arguments'] = $array300;
$arguments299['addQueryStringMethod'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'addQueryStringMethod', $renderingContext);
$arguments299['section'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'section', $renderingContext);
$arguments299['additionalAttributes'] = NULL;
$arguments299['data'] = NULL;
$arguments299['action'] = NULL;
$arguments299['format'] = '';
$arguments299['ajax'] = false;
$arguments299['class'] = NULL;
$arguments299['dir'] = NULL;
$arguments299['id'] = NULL;
$arguments299['lang'] = NULL;
$arguments299['style'] = NULL;
$arguments299['title'] = NULL;
$arguments299['accesskey'] = NULL;
$arguments299['tabindex'] = NULL;
$arguments299['onclick'] = NULL;
$arguments299['name'] = NULL;
$arguments299['rel'] = NULL;
$arguments299['rev'] = NULL;
$arguments299['target'] = NULL;
$renderChildrenClosure301 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output302 = '';

$output302 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments303 = array();
$arguments303['value'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext);
$arguments303['keepQuotes'] = false;
$arguments303['encoding'] = NULL;
$arguments303['doubleEncode'] = true;
$renderChildrenClosure304 = function() {return NULL;};
$value305 = ($arguments303['value'] !== NULL ? $arguments303['value'] : $renderChildrenClosure304());

$output302 .= (!is_string($value305) ? $value305 : htmlspecialchars($value305, ($arguments303['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments303['encoding'] !== NULL ? $arguments303['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments303['doubleEncode']));

$output302 .= '
									';
return $output302;
};
$viewHelper306 = $self->getViewHelper('$viewHelper306', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper306->setArguments($arguments299);
$viewHelper306->setRenderingContext($renderingContext);
$viewHelper306->setRenderChildrenClosure($renderChildrenClosure301);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output298 .= $viewHelper306->initializeArgumentsAndRender();

$output298 .= '
								';
return $output298;
};

$output295 .= TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper::renderStatic($arguments296, $renderChildrenClosure297, $renderingContext);

$output295 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper
$arguments307 = array();
$renderChildrenClosure308 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output309 = '';

$output309 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments310 = array();
$arguments310['addQueryStringMethod'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'addQueryStringMethod', $renderingContext);
$arguments310['section'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'section', $renderingContext);
$arguments310['additionalAttributes'] = NULL;
$arguments310['data'] = NULL;
$arguments310['action'] = NULL;
$arguments310['arguments'] = array (
);
$arguments310['format'] = '';
$arguments310['ajax'] = false;
$arguments310['class'] = NULL;
$arguments310['dir'] = NULL;
$arguments310['id'] = NULL;
$arguments310['lang'] = NULL;
$arguments310['style'] = NULL;
$arguments310['title'] = NULL;
$arguments310['accesskey'] = NULL;
$arguments310['tabindex'] = NULL;
$arguments310['onclick'] = NULL;
$arguments310['name'] = NULL;
$arguments310['rel'] = NULL;
$arguments310['rev'] = NULL;
$arguments310['target'] = NULL;
$renderChildrenClosure311 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output312 = '';

$output312 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments313 = array();
$arguments313['value'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext);
$arguments313['keepQuotes'] = false;
$arguments313['encoding'] = NULL;
$arguments313['doubleEncode'] = true;
$renderChildrenClosure314 = function() {return NULL;};
$value315 = ($arguments313['value'] !== NULL ? $arguments313['value'] : $renderChildrenClosure314());

$output312 .= (!is_string($value315) ? $value315 : htmlspecialchars($value315, ($arguments313['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments313['encoding'] !== NULL ? $arguments313['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments313['doubleEncode']));

$output312 .= '
									';
return $output312;
};
$viewHelper316 = $self->getViewHelper('$viewHelper316', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper316->setArguments($arguments310);
$viewHelper316->setRenderingContext($renderingContext);
$viewHelper316->setRenderChildrenClosure($renderChildrenClosure311);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output309 .= $viewHelper316->initializeArgumentsAndRender();

$output309 .= '
								';
return $output309;
};

$output295 .= TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper::renderStatic($arguments307, $renderChildrenClosure308, $renderingContext);

$output295 .= '
							';
return $output295;
};
$arguments293['__thenClosure'] = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output317 = '';

$output317 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments318 = array();
// Rendering Array
$array319 = array();
$array319['currentPage'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext);
$arguments318['arguments'] = $array319;
$arguments318['addQueryStringMethod'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'addQueryStringMethod', $renderingContext);
$arguments318['section'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'section', $renderingContext);
$arguments318['additionalAttributes'] = NULL;
$arguments318['data'] = NULL;
$arguments318['action'] = NULL;
$arguments318['format'] = '';
$arguments318['ajax'] = false;
$arguments318['class'] = NULL;
$arguments318['dir'] = NULL;
$arguments318['id'] = NULL;
$arguments318['lang'] = NULL;
$arguments318['style'] = NULL;
$arguments318['title'] = NULL;
$arguments318['accesskey'] = NULL;
$arguments318['tabindex'] = NULL;
$arguments318['onclick'] = NULL;
$arguments318['name'] = NULL;
$arguments318['rel'] = NULL;
$arguments318['rev'] = NULL;
$arguments318['target'] = NULL;
$renderChildrenClosure320 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output321 = '';

$output321 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments322 = array();
$arguments322['value'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext);
$arguments322['keepQuotes'] = false;
$arguments322['encoding'] = NULL;
$arguments322['doubleEncode'] = true;
$renderChildrenClosure323 = function() {return NULL;};
$value324 = ($arguments322['value'] !== NULL ? $arguments322['value'] : $renderChildrenClosure323());

$output321 .= (!is_string($value324) ? $value324 : htmlspecialchars($value324, ($arguments322['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments322['encoding'] !== NULL ? $arguments322['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments322['doubleEncode']));

$output321 .= '
									';
return $output321;
};
$viewHelper325 = $self->getViewHelper('$viewHelper325', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper325->setArguments($arguments318);
$viewHelper325->setRenderingContext($renderingContext);
$viewHelper325->setRenderChildrenClosure($renderChildrenClosure320);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output317 .= $viewHelper325->initializeArgumentsAndRender();

$output317 .= '
								';
return $output317;
};
$arguments293['__elseClosure'] = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output326 = '';

$output326 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments327 = array();
$arguments327['addQueryStringMethod'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'addQueryStringMethod', $renderingContext);
$arguments327['section'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'section', $renderingContext);
$arguments327['additionalAttributes'] = NULL;
$arguments327['data'] = NULL;
$arguments327['action'] = NULL;
$arguments327['arguments'] = array (
);
$arguments327['format'] = '';
$arguments327['ajax'] = false;
$arguments327['class'] = NULL;
$arguments327['dir'] = NULL;
$arguments327['id'] = NULL;
$arguments327['lang'] = NULL;
$arguments327['style'] = NULL;
$arguments327['title'] = NULL;
$arguments327['accesskey'] = NULL;
$arguments327['tabindex'] = NULL;
$arguments327['onclick'] = NULL;
$arguments327['name'] = NULL;
$arguments327['rel'] = NULL;
$arguments327['rev'] = NULL;
$arguments327['target'] = NULL;
$renderChildrenClosure328 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output329 = '';

$output329 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments330 = array();
$arguments330['value'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext);
$arguments330['keepQuotes'] = false;
$arguments330['encoding'] = NULL;
$arguments330['doubleEncode'] = true;
$renderChildrenClosure331 = function() {return NULL;};
$value332 = ($arguments330['value'] !== NULL ? $arguments330['value'] : $renderChildrenClosure331());

$output329 .= (!is_string($value332) ? $value332 : htmlspecialchars($value332, ($arguments330['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments330['encoding'] !== NULL ? $arguments330['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments330['doubleEncode']));

$output329 .= '
									';
return $output329;
};
$viewHelper333 = $self->getViewHelper('$viewHelper333', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper333->setArguments($arguments327);
$viewHelper333->setRenderingContext($renderingContext);
$viewHelper333->setRenderChildrenClosure($renderChildrenClosure328);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output326 .= $viewHelper333->initializeArgumentsAndRender();

$output326 .= '
								';
return $output326;
};

$output292 .= TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper::renderStatic($arguments293, $renderChildrenClosure294, $renderingContext);

$output292 .= '
						</li>
					';
return $output292;
};

$output278 .= TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper::renderStatic($arguments290, $renderChildrenClosure291, $renderingContext);

$output278 .= '
				';
return $output278;
};
$arguments276['__thenClosure'] = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output334 = '';

$output334 .= '
						<li class="active">
							';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments335 = array();
// Rendering Array
$array336 = array();
$array336['currentPage'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext);
$arguments335['arguments'] = $array336;
$arguments335['addQueryStringMethod'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'addQueryStringMethod', $renderingContext);
$arguments335['section'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'section', $renderingContext);
$arguments335['additionalAttributes'] = NULL;
$arguments335['data'] = NULL;
$arguments335['action'] = NULL;
$arguments335['format'] = '';
$arguments335['ajax'] = false;
$arguments335['class'] = NULL;
$arguments335['dir'] = NULL;
$arguments335['id'] = NULL;
$arguments335['lang'] = NULL;
$arguments335['style'] = NULL;
$arguments335['title'] = NULL;
$arguments335['accesskey'] = NULL;
$arguments335['tabindex'] = NULL;
$arguments335['onclick'] = NULL;
$arguments335['name'] = NULL;
$arguments335['rel'] = NULL;
$arguments335['rev'] = NULL;
$arguments335['target'] = NULL;
$renderChildrenClosure337 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output338 = '';

$output338 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments339 = array();
$arguments339['value'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext);
$arguments339['keepQuotes'] = false;
$arguments339['encoding'] = NULL;
$arguments339['doubleEncode'] = true;
$renderChildrenClosure340 = function() {return NULL;};
$value341 = ($arguments339['value'] !== NULL ? $arguments339['value'] : $renderChildrenClosure340());

$output338 .= (!is_string($value341) ? $value341 : htmlspecialchars($value341, ($arguments339['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments339['encoding'] !== NULL ? $arguments339['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments339['doubleEncode']));

$output338 .= '
							';
return $output338;
};
$viewHelper342 = $self->getViewHelper('$viewHelper342', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper342->setArguments($arguments335);
$viewHelper342->setRenderingContext($renderingContext);
$viewHelper342->setRenderChildrenClosure($renderChildrenClosure337);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output334 .= $viewHelper342->initializeArgumentsAndRender();

$output334 .= '
						</li>
					';
return $output334;
};
$arguments276['__elseClosure'] = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output343 = '';

$output343 .= '
						<li>
							';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments344 = array();
// Rendering Boolean node
$arguments344['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('>', \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext), 1);
$arguments344['then'] = NULL;
$arguments344['else'] = NULL;
$renderChildrenClosure345 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output346 = '';

$output346 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper
$arguments347 = array();
$renderChildrenClosure348 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output349 = '';

$output349 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments350 = array();
// Rendering Array
$array351 = array();
$array351['currentPage'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext);
$arguments350['arguments'] = $array351;
$arguments350['addQueryStringMethod'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'addQueryStringMethod', $renderingContext);
$arguments350['section'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'section', $renderingContext);
$arguments350['additionalAttributes'] = NULL;
$arguments350['data'] = NULL;
$arguments350['action'] = NULL;
$arguments350['format'] = '';
$arguments350['ajax'] = false;
$arguments350['class'] = NULL;
$arguments350['dir'] = NULL;
$arguments350['id'] = NULL;
$arguments350['lang'] = NULL;
$arguments350['style'] = NULL;
$arguments350['title'] = NULL;
$arguments350['accesskey'] = NULL;
$arguments350['tabindex'] = NULL;
$arguments350['onclick'] = NULL;
$arguments350['name'] = NULL;
$arguments350['rel'] = NULL;
$arguments350['rev'] = NULL;
$arguments350['target'] = NULL;
$renderChildrenClosure352 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output353 = '';

$output353 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments354 = array();
$arguments354['value'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext);
$arguments354['keepQuotes'] = false;
$arguments354['encoding'] = NULL;
$arguments354['doubleEncode'] = true;
$renderChildrenClosure355 = function() {return NULL;};
$value356 = ($arguments354['value'] !== NULL ? $arguments354['value'] : $renderChildrenClosure355());

$output353 .= (!is_string($value356) ? $value356 : htmlspecialchars($value356, ($arguments354['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments354['encoding'] !== NULL ? $arguments354['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments354['doubleEncode']));

$output353 .= '
									';
return $output353;
};
$viewHelper357 = $self->getViewHelper('$viewHelper357', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper357->setArguments($arguments350);
$viewHelper357->setRenderingContext($renderingContext);
$viewHelper357->setRenderChildrenClosure($renderChildrenClosure352);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output349 .= $viewHelper357->initializeArgumentsAndRender();

$output349 .= '
								';
return $output349;
};

$output346 .= TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper::renderStatic($arguments347, $renderChildrenClosure348, $renderingContext);

$output346 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper
$arguments358 = array();
$renderChildrenClosure359 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output360 = '';

$output360 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments361 = array();
$arguments361['addQueryStringMethod'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'addQueryStringMethod', $renderingContext);
$arguments361['section'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'section', $renderingContext);
$arguments361['additionalAttributes'] = NULL;
$arguments361['data'] = NULL;
$arguments361['action'] = NULL;
$arguments361['arguments'] = array (
);
$arguments361['format'] = '';
$arguments361['ajax'] = false;
$arguments361['class'] = NULL;
$arguments361['dir'] = NULL;
$arguments361['id'] = NULL;
$arguments361['lang'] = NULL;
$arguments361['style'] = NULL;
$arguments361['title'] = NULL;
$arguments361['accesskey'] = NULL;
$arguments361['tabindex'] = NULL;
$arguments361['onclick'] = NULL;
$arguments361['name'] = NULL;
$arguments361['rel'] = NULL;
$arguments361['rev'] = NULL;
$arguments361['target'] = NULL;
$renderChildrenClosure362 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output363 = '';

$output363 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments364 = array();
$arguments364['value'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext);
$arguments364['keepQuotes'] = false;
$arguments364['encoding'] = NULL;
$arguments364['doubleEncode'] = true;
$renderChildrenClosure365 = function() {return NULL;};
$value366 = ($arguments364['value'] !== NULL ? $arguments364['value'] : $renderChildrenClosure365());

$output363 .= (!is_string($value366) ? $value366 : htmlspecialchars($value366, ($arguments364['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments364['encoding'] !== NULL ? $arguments364['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments364['doubleEncode']));

$output363 .= '
									';
return $output363;
};
$viewHelper367 = $self->getViewHelper('$viewHelper367', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper367->setArguments($arguments361);
$viewHelper367->setRenderingContext($renderingContext);
$viewHelper367->setRenderChildrenClosure($renderChildrenClosure362);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output360 .= $viewHelper367->initializeArgumentsAndRender();

$output360 .= '
								';
return $output360;
};

$output346 .= TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper::renderStatic($arguments358, $renderChildrenClosure359, $renderingContext);

$output346 .= '
							';
return $output346;
};
$arguments344['__thenClosure'] = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output368 = '';

$output368 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments369 = array();
// Rendering Array
$array370 = array();
$array370['currentPage'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext);
$arguments369['arguments'] = $array370;
$arguments369['addQueryStringMethod'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'addQueryStringMethod', $renderingContext);
$arguments369['section'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'section', $renderingContext);
$arguments369['additionalAttributes'] = NULL;
$arguments369['data'] = NULL;
$arguments369['action'] = NULL;
$arguments369['format'] = '';
$arguments369['ajax'] = false;
$arguments369['class'] = NULL;
$arguments369['dir'] = NULL;
$arguments369['id'] = NULL;
$arguments369['lang'] = NULL;
$arguments369['style'] = NULL;
$arguments369['title'] = NULL;
$arguments369['accesskey'] = NULL;
$arguments369['tabindex'] = NULL;
$arguments369['onclick'] = NULL;
$arguments369['name'] = NULL;
$arguments369['rel'] = NULL;
$arguments369['rev'] = NULL;
$arguments369['target'] = NULL;
$renderChildrenClosure371 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output372 = '';

$output372 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments373 = array();
$arguments373['value'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext);
$arguments373['keepQuotes'] = false;
$arguments373['encoding'] = NULL;
$arguments373['doubleEncode'] = true;
$renderChildrenClosure374 = function() {return NULL;};
$value375 = ($arguments373['value'] !== NULL ? $arguments373['value'] : $renderChildrenClosure374());

$output372 .= (!is_string($value375) ? $value375 : htmlspecialchars($value375, ($arguments373['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments373['encoding'] !== NULL ? $arguments373['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments373['doubleEncode']));

$output372 .= '
									';
return $output372;
};
$viewHelper376 = $self->getViewHelper('$viewHelper376', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper376->setArguments($arguments369);
$viewHelper376->setRenderingContext($renderingContext);
$viewHelper376->setRenderChildrenClosure($renderChildrenClosure371);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output368 .= $viewHelper376->initializeArgumentsAndRender();

$output368 .= '
								';
return $output368;
};
$arguments344['__elseClosure'] = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output377 = '';

$output377 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments378 = array();
$arguments378['addQueryStringMethod'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'addQueryStringMethod', $renderingContext);
$arguments378['section'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'section', $renderingContext);
$arguments378['additionalAttributes'] = NULL;
$arguments378['data'] = NULL;
$arguments378['action'] = NULL;
$arguments378['arguments'] = array (
);
$arguments378['format'] = '';
$arguments378['ajax'] = false;
$arguments378['class'] = NULL;
$arguments378['dir'] = NULL;
$arguments378['id'] = NULL;
$arguments378['lang'] = NULL;
$arguments378['style'] = NULL;
$arguments378['title'] = NULL;
$arguments378['accesskey'] = NULL;
$arguments378['tabindex'] = NULL;
$arguments378['onclick'] = NULL;
$arguments378['name'] = NULL;
$arguments378['rel'] = NULL;
$arguments378['rev'] = NULL;
$arguments378['target'] = NULL;
$renderChildrenClosure379 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output380 = '';

$output380 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments381 = array();
$arguments381['value'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext);
$arguments381['keepQuotes'] = false;
$arguments381['encoding'] = NULL;
$arguments381['doubleEncode'] = true;
$renderChildrenClosure382 = function() {return NULL;};
$value383 = ($arguments381['value'] !== NULL ? $arguments381['value'] : $renderChildrenClosure382());

$output380 .= (!is_string($value383) ? $value383 : htmlspecialchars($value383, ($arguments381['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments381['encoding'] !== NULL ? $arguments381['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments381['doubleEncode']));

$output380 .= '
									';
return $output380;
};
$viewHelper384 = $self->getViewHelper('$viewHelper384', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper384->setArguments($arguments378);
$viewHelper384->setRenderingContext($renderingContext);
$viewHelper384->setRenderChildrenClosure($renderChildrenClosure379);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output377 .= $viewHelper384->initializeArgumentsAndRender();

$output377 .= '
								';
return $output377;
};

$output343 .= TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper::renderStatic($arguments344, $renderChildrenClosure345, $renderingContext);

$output343 .= '
						</li>
					';
return $output343;
};

$output275 .= TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper::renderStatic($arguments276, $renderChildrenClosure277, $renderingContext);

$output275 .= '
			';
return $output275;
};

$output224 .= TYPO3\CMS\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments273, $renderChildrenClosure274, $renderingContext);

$output224 .= '
			';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments385 = array();
// Rendering Boolean node
$arguments385['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('pagination'), 'hasMorePages', $renderingContext));
$arguments385['then'] = NULL;
$arguments385['else'] = NULL;
$renderChildrenClosure386 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
return '
				<li>...</li>
			';
};

$output224 .= TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper::renderStatic($arguments385, $renderChildrenClosure386, $renderingContext);

$output224 .= '
			';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments387 = array();
// Rendering Boolean node
$arguments387['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('<', \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('pagination'), 'displayRangeEnd', $renderingContext), \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('pagination'), 'numberOfPages', $renderingContext));
$arguments387['then'] = NULL;
$arguments387['else'] = NULL;
$renderChildrenClosure388 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output389 = '';

$output389 .= '
				<li class="last">
					';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments390 = array();
// Rendering Array
$array391 = array();
$array391['currentPage'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('pagination'), 'numberOfPages', $renderingContext);
$arguments390['arguments'] = $array391;
$arguments390['addQueryStringMethod'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'addQueryStringMethod', $renderingContext);
$arguments390['section'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'section', $renderingContext);
$arguments390['additionalAttributes'] = NULL;
$arguments390['data'] = NULL;
$arguments390['action'] = NULL;
$arguments390['format'] = '';
$arguments390['ajax'] = false;
$arguments390['class'] = NULL;
$arguments390['dir'] = NULL;
$arguments390['id'] = NULL;
$arguments390['lang'] = NULL;
$arguments390['style'] = NULL;
$arguments390['title'] = NULL;
$arguments390['accesskey'] = NULL;
$arguments390['tabindex'] = NULL;
$arguments390['onclick'] = NULL;
$arguments390['name'] = NULL;
$arguments390['rel'] = NULL;
$arguments390['rev'] = NULL;
$arguments390['target'] = NULL;
$renderChildrenClosure392 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output393 = '';

$output393 .= '
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments394 = array();
$arguments394['value'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('pagination'), 'numberOfPages', $renderingContext);
$arguments394['keepQuotes'] = false;
$arguments394['encoding'] = NULL;
$arguments394['doubleEncode'] = true;
$renderChildrenClosure395 = function() {return NULL;};
$value396 = ($arguments394['value'] !== NULL ? $arguments394['value'] : $renderChildrenClosure395());

$output393 .= (!is_string($value396) ? $value396 : htmlspecialchars($value396, ($arguments394['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments394['encoding'] !== NULL ? $arguments394['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments394['doubleEncode']));

$output393 .= '
					';
return $output393;
};
$viewHelper397 = $self->getViewHelper('$viewHelper397', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper397->setArguments($arguments390);
$viewHelper397->setRenderingContext($renderingContext);
$viewHelper397->setRenderChildrenClosure($renderChildrenClosure392);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output389 .= $viewHelper397->initializeArgumentsAndRender();

$output389 .= '
				</li>
			';
return $output389;
};

$output224 .= TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper::renderStatic($arguments387, $renderChildrenClosure388, $renderingContext);

$output224 .= '
			';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments398 = array();
// Rendering Boolean node
$arguments398['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('pagination'), 'nextPage', $renderingContext));
$arguments398['then'] = NULL;
$arguments398['else'] = NULL;
$renderChildrenClosure399 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output400 = '';

$output400 .= '
				<li class="next">
					';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments401 = array();
// Rendering Array
$array402 = array();
$array402['currentPage'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('pagination'), 'nextPage', $renderingContext);
$arguments401['arguments'] = $array402;
$arguments401['addQueryStringMethod'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'addQueryStringMethod', $renderingContext);
$arguments401['section'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('configuration'), 'section', $renderingContext);
// Rendering Array
$array403 = array();
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments404 = array();
$arguments404['key'] = 'widget.pagination.next';
$arguments404['id'] = NULL;
$arguments404['default'] = NULL;
$arguments404['htmlEscape'] = NULL;
$arguments404['arguments'] = NULL;
$arguments404['extensionName'] = NULL;
$renderChildrenClosure405 = function() {return NULL;};
$array403['aria-label'] = TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper::renderStatic($arguments404, $renderChildrenClosure405, $renderingContext);
$arguments401['additionalAttributes'] = $array403;
$arguments401['data'] = NULL;
$arguments401['action'] = NULL;
$arguments401['format'] = '';
$arguments401['ajax'] = false;
$arguments401['class'] = NULL;
$arguments401['dir'] = NULL;
$arguments401['id'] = NULL;
$arguments401['lang'] = NULL;
$arguments401['style'] = NULL;
$arguments401['title'] = NULL;
$arguments401['accesskey'] = NULL;
$arguments401['tabindex'] = NULL;
$arguments401['onclick'] = NULL;
$arguments401['name'] = NULL;
$arguments401['rel'] = NULL;
$arguments401['rev'] = NULL;
$arguments401['target'] = NULL;
$renderChildrenClosure406 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
return '
						<span aria-hidden="true">&raquo;</span>
					';
};
$viewHelper407 = $self->getViewHelper('$viewHelper407', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper407->setArguments($arguments401);
$viewHelper407->setRenderingContext($renderingContext);
$viewHelper407->setRenderChildrenClosure($renderChildrenClosure406);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output400 .= $viewHelper407->initializeArgumentsAndRender();

$output400 .= '
				</li>
			';
return $output400;
};

$output224 .= TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper::renderStatic($arguments398, $renderChildrenClosure399, $renderingContext);

$output224 .= '
		</ul>
	</nav>
';
return $output224;
};

$output203 .= '';

$output203 .= '

';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\SectionViewHelper
$arguments408 = array();
$arguments408['name'] = 'title';
$renderChildrenClosure409 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output410 = '';

$output410 .= '
	';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ForViewHelper
$arguments411 = array();
$arguments411['each'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('pagination'), 'pages', $renderingContext);
$arguments411['as'] = 'page';
$arguments411['key'] = '';
$arguments411['reverse'] = false;
$arguments411['iteration'] = NULL;
$renderChildrenClosure412 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output413 = '';

$output413 .= '
		';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments414 = array();
// Rendering Boolean node
$arguments414['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'isCurrent', $renderingContext));
$arguments414['then'] = NULL;
$arguments414['else'] = NULL;
$renderChildrenClosure415 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output416 = '';

$output416 .= '
			';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments417 = array();
// Rendering Boolean node
$arguments417['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('>', \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext), 1);
$arguments417['then'] = NULL;
$arguments417['else'] = NULL;
$renderChildrenClosure418 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output419 = '';

$output419 .= '
				';
// Rendering ViewHelper TYPO3\T3extblog\ViewHelpers\Frontend\TitleTagViewHelper
$arguments420 = array();
$arguments420['prepend'] = true;
$arguments420['searchTitle'] = NULL;
$renderChildrenClosure421 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
$output422 = '';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments423 = array();
$arguments423['key'] = 'widget.pagination.page';
$arguments423['id'] = NULL;
$arguments423['default'] = NULL;
$arguments423['htmlEscape'] = NULL;
$arguments423['arguments'] = NULL;
$arguments423['extensionName'] = NULL;
$renderChildrenClosure424 = function() {return NULL;};

$output422 .= TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper::renderStatic($arguments423, $renderChildrenClosure424, $renderingContext);

$output422 .= ' ';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments425 = array();
$arguments425['value'] = \TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($currentVariableContainer->getOrNull('page'), 'number', $renderingContext);
$arguments425['keepQuotes'] = false;
$arguments425['encoding'] = NULL;
$arguments425['doubleEncode'] = true;
$renderChildrenClosure426 = function() {return NULL;};
$value427 = ($arguments425['value'] !== NULL ? $arguments425['value'] : $renderChildrenClosure426());

$output422 .= (!is_string($value427) ? $value427 : htmlspecialchars($value427, ($arguments425['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments425['encoding'] !== NULL ? $arguments425['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments425['doubleEncode']));

$output422 .= ' / ';
return $output422;
};
$viewHelper428 = $self->getViewHelper('$viewHelper428', $renderingContext, 'TYPO3\T3extblog\ViewHelpers\Frontend\TitleTagViewHelper');
$viewHelper428->setArguments($arguments420);
$viewHelper428->setRenderingContext($renderingContext);
$viewHelper428->setRenderChildrenClosure($renderChildrenClosure421);
// End of ViewHelper TYPO3\T3extblog\ViewHelpers\Frontend\TitleTagViewHelper

$output419 .= $viewHelper428->initializeArgumentsAndRender();

$output419 .= '
			';
return $output419;
};

$output416 .= TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper::renderStatic($arguments417, $renderChildrenClosure418, $renderingContext);

$output416 .= '
		';
return $output416;
};

$output413 .= TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper::renderStatic($arguments414, $renderChildrenClosure415, $renderingContext);

$output413 .= '
	';
return $output413;
};

$output410 .= TYPO3\CMS\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments411, $renderChildrenClosure412, $renderingContext);

$output410 .= '
';
return $output410;
};

$output203 .= '';

$output203 .= '
';


return $output203;
}


}
#1468572057    135682    