<?php
class FluidCache_Standalone_template_file_4_about_me_5ea91c4407fbcf801d5eedea337b7814bc7fe417 extends \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate {

public function getVariableContainer() {
	// @todo
	return new \TYPO3\CMS\Fluid\Core\ViewHelper\TemplateVariableContainer();
}
public function getLayoutName(\TYPO3\CMS\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();

return NULL;
}
public function hasLayout() {
return FALSE;
}

/**
 * Main Render function
 */
public function render(\TYPO3\CMS\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();

$output0 = '';

$output0 .= '

<div class="well">
    <div class="teaser-home">
        <img class="img-rounded img-responsive img-center" width="140" height="140" src="fileadmin/Images/dsc0167001.jpg" />
        <h4>Über mich</h4>
        <p> Ich bin Karina, ich bin 27 und wohne in der schönsten Stadt der Welt <3.Ich liebe es, schöne Dinge selber herzustellen :) Sei es mit der Nähmaschine, mit Strick- oder mit Häkelnadeln. Es macht mir alles unglaublich viel Spaß <3 Ich entdecke ständig neue Anleitungen und Anregungen für potentielle Projekte und möchte diese alle festhalten und mit der ganzen Welt teilen! Viel Spaß beim Lesen und Schauen!</p>
        <p>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Link\TypolinkViewHelper
$arguments1 = array();
$arguments1['class'] = 'btn btn-default';
$arguments1['parameter'] = '3';
$arguments1['target'] = '';
$arguments1['title'] = '';
$arguments1['additionalParams'] = '';
$arguments1['additionalAttributes'] = array (
);
$renderChildrenClosure2 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
return 'Mehr lesen &raquo;';
};

$output0 .= TYPO3\CMS\Fluid\ViewHelpers\Link\TypolinkViewHelper::renderStatic($arguments1, $renderChildrenClosure2, $renderingContext);

$output0 .= '</p>
    </div>
</div>';


return $output0;
}


}
#1468514224    2149      