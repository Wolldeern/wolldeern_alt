<?php
class FluidCache_Standalone_template_file_backend_layout_1_5b1b98d3e08318f5bc3fe4bcb1f7b1980b701eba extends \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate {

public function getVariableContainer() {
	// @todo
	return new \TYPO3\CMS\Fluid\Core\ViewHelper\TemplateVariableContainer();
}
public function getLayoutName(\TYPO3\CMS\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();

return NULL;
}
public function hasLayout() {
return FALSE;
}

/**
 * Main Render function
 */
public function render(\TYPO3\CMS\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();

$output0 = '';

$output0 .= '

<div class="container">
    ';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper
$arguments1 = array();
$arguments1['partial'] = 'Header.html';
$arguments1['arguments'] = $currentVariableContainer->getOrNull('_all');
$arguments1['section'] = NULL;
$arguments1['optional'] = false;
$renderChildrenClosure2 = function() {return NULL;};

$output0 .= TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper::renderStatic($arguments1, $renderChildrenClosure2, $renderingContext);

$output0 .= '
    <div class="page-content ">
        ';
// Rendering ViewHelper FluidTYPO3\Vhs\ViewHelpers\Content\RenderViewHelper
$arguments3 = array();
$arguments3['column'] = '0';
$arguments3['order'] = 'sorting';
$arguments3['sortDirection'] = 'ASC';
$arguments3['pageUid'] = 0;
$arguments3['contentUids'] = NULL;
$arguments3['sectionIndexOnly'] = false;
$arguments3['loadRegister'] = NULL;
$arguments3['render'] = true;
$arguments3['hideUntranslated'] = false;
$arguments3['limit'] = NULL;
$arguments3['slide'] = 0;
$arguments3['slideCollect'] = 0;
$arguments3['slideCollectReverse'] = 0;
$arguments3['as'] = NULL;
$renderChildrenClosure4 = function() {return NULL;};
$viewHelper5 = $self->getViewHelper('$viewHelper5', $renderingContext, 'FluidTYPO3\Vhs\ViewHelpers\Content\RenderViewHelper');
$viewHelper5->setArguments($arguments3);
$viewHelper5->setRenderingContext($renderingContext);
$viewHelper5->setRenderChildrenClosure($renderChildrenClosure4);
// End of ViewHelper FluidTYPO3\Vhs\ViewHelpers\Content\RenderViewHelper

$output0 .= $viewHelper5->initializeArgumentsAndRender();

$output0 .= '
        <!--<div class="col-sm-8 col-xs-12">-->
            <!--<div class="date-headline">-->
                <!--<div class="date-headline-inner">-->
                    <!--17.06.2016-->
                <!--</div>-->
            <!--</div>-->
            <!--<h2 class="text-center">SAMPLE POST</h2>-->
            <!--<div class="text-center">3 Kommentare Kommentar schreiben</div>-->
            <!--<img class="img-responsive img-center" src="fileadmin/Images/Kissen.jpg" />-->
            <!--<p>-->
                <!--Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam-->
                <!--nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam-->
                <!--erat, sed diam voluptua.-->
            <!--</p>-->
            <!--<br>-->

            <!--<div class="date-headline">-->
                <!--<div class="date-headline-inner">-->
                    <!--';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Link\TypolinkViewHelper
$arguments6 = array();
$arguments6['parameter'] = '4';
$arguments6['target'] = '';
$arguments6['class'] = '';
$arguments6['title'] = '';
$arguments6['additionalParams'] = '';
$arguments6['additionalAttributes'] = array (
);
$renderChildrenClosure7 = function() use ($renderingContext, $self) {
$currentVariableContainer = $renderingContext->getTemplateVariableContainer();
return '17.06.2016';
};

$output0 .= TYPO3\CMS\Fluid\ViewHelpers\Link\TypolinkViewHelper::renderStatic($arguments6, $renderChildrenClosure7, $renderingContext);

$output0 .= '-->
                <!--</div>-->
            <!--</div>-->
            <!--<h2 class="text-center">SAMPLE POST</h2>-->
            <!--<div class="text-center">3 Kommentare Kommentar schreiben</div>-->
            <!--<img class="img-responsive img-center" src="fileadmin/Images/Kissen.jpg" />-->
            <!--<p>-->
                <!--Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam-->
                <!--nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam-->
                <!--erat, sed diam voluptua.-->
            <!--</p>-->
        <!--</div>-->
        ';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper
$arguments8 = array();
$arguments8['partial'] = 'Footer.html';
$arguments8['arguments'] = $currentVariableContainer->getOrNull('_all');
$arguments8['section'] = NULL;
$arguments8['optional'] = false;
$renderChildrenClosure9 = function() {return NULL;};

$output0 .= TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper::renderStatic($arguments8, $renderChildrenClosure9, $renderingContext);

$output0 .= '
    </div>
</div>
';


return $output0;
}


}
#1468572057    5057      