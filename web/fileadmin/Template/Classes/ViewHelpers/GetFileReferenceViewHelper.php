<?php
namespace Karina\viewhelpers;
use TYPO3\CMS\Core\Resource\FileReference;

/**
 * This ViewHelper returns s file reference in as variable defined in as
 *
 */
class getfilereferenceviewhelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {
    /**
     * @var \TYPO3\CMS\Core\Resource\FileRepository
     * @inject
     */
    protected $fileReferenceRepository;


    /**
     * @param string $as
     * @param string $tableName
     * @param string $fieldName
     * @param int $uid
     * @return string
     */
    public function render($as,$tableName, $fieldName, $uid ) {


        $references = $this->fileReferenceRepository->findByRelation($tableName,$fieldName,$uid);

        $output = '';

        foreach($references as $reference){
            $this->templateVariableContainer->add($as, $reference);
            $output .= $this->renderChildren();
            $this->templateVariableContainer->remove($as);
        }


        return $output;

    }

}
