#----------------------------------------
# Includes
#----------------------------------------
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/template/configuration/typoscript/config/config.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/template/configuration/typoscript/page/type-0.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/template/configuration/tsconfig/page.ts">

#----------------------------------------
# Extensions
#----------------------------------------
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/template/configuration/typoscript/extensions/gridelements/setup.txt">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/template/configuration/typoscript/extensions/t3extblog/setup.txt">


#----------------------------------------
# Fluid Styled Content
#----------------------------------------
lib.fluidContent.templateRootPaths.30 = fileadmin/template/resources/templates/fluid_styled_content/templates
lib.fluidContent.partialRootPaths.30 = fileadmin/template/resources/templates/fluid_styled_content/partials