#----------------------------------------
# Global configuration
#----------------------------------------
config {
	# Paths
	absRefPrefix = auto
	contentObjectExceptionHandler = 0

	# Encoding
	metaCharset = utf-8

	# Routing
	simulateStaticDocuments = 0
	tx_realurl_enable = 1
	linkVars = L

	# Head
	removeDefaultJS = 0
	disablePrefixComment = 1

	# Spam
	spamProtectEmailAddresses = 2<
	spamProtectEmailAddresses_atSubst = <span style="display:none">&nbsp;</span>&#064;<span style="display:none">&nbsp;</span>

	# Language
	sys_language_uid = 0
	language = de
	locale_all = de_DE
	htmlTag_setParams = lang="de-DE"

	# Title
	noPageTitle = 2

	# Copyright
	headerComment = Karina developed this website.

	#    # Concatenate
	#    concatenateCss = 1
	#    concatenateJs = 1
	#
	#    # Compress
	#    compressCss = 1
	#    compressJs = 1

	no_cache = 1
}

tx_news.templateLayouts {
	1 = Default
	99 = Startseite
}