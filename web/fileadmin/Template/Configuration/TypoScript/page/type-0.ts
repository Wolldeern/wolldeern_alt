#----------------------------------------
# Page (type 0)
#----------------------------------------

page = PAGE
page {
	typeNum = 0

	meta.keywords.field = keywords
	meta.description.field = description

	headerData {
		10 = TEXT
		10.value =  <meta name="viewport" content="width=device-width, initial-scale=1">
		20 = TEXT
		20.value = <meta http-equiv="X-UA-Compatible" content="IE=edge">
		30 = TEXT
		30.value = <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,400,300,600' rel='stylesheet' type='text/css'>
		40 = TEXT
		40.value = <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,300' rel='stylesheet' type='text/css'>
		50 = TEXT
		50.value = <meta name="viewport" content="width=device-width, initial-scale=1">
	}

	includeCSS {
		1_styles = fileadmin/template/resources/styles/style.css
	}
	includeJSFooter {
		10_jquery = fileadmin/template/bootstrap/js/jquery.min.js
		20_bootstrap = fileadmin/template/bootstrap/js/bootstrap.min.js
	}

	10 = CASE
	10 {
		key.data = levelfield:-2, backend_layout_next_level, slide
		key.override.field = backend_layout

		default = TEXT
		default.value = No backend_layout selected
		default.wrap = <strong>|</strong>

		1 = FLUIDTEMPLATE
		1 {
			file = fileadmin/template/resources/templates/backend_layout-1.html
			partialRootPath = fileadmin/template/resources/partials
			settings {
				pidHome = {$pid.home}
				pidPrimary = {$pid.primary}
				pidSearch = {$pid.search}
				pidFooter = {$pid.footer}
				pidSocial = {$pid.social}
				pidAbout = {$pid.about}
			}
			variables {
			}
		}
	}
}