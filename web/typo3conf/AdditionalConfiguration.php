<?php

/**
 * Live configuration
 */

$GLOBALS['TYPO3_CONF_VARS']['BE']['installToolPassword'] = md5('local');
$GLOBALS['TYPO3_CONF_VARS']['BE']['lockSSL']  = 0;

#$GLOBALS['TYPO3_CONF_VARS']['DB']['host'] = '127.0.0.1';
$GLOBALS['TYPO3_CONF_VARS']['DB']['database'] = 'karina_database';
$GLOBALS['TYPO3_CONF_VARS']['DB']['host'] = 'localhost';
$GLOBALS['TYPO3_CONF_VARS']['DB']['username'] = 'root';
$GLOBALS['TYPO3_CONF_VARS']['DB']['socket'] = '';
$GLOBALS['TYPO3_CONF_VARS']['DB']['password'] = 'root';

$GLOBALS['TYPO3_CONF_VARS']['GFX']['colorspace'] = 'sRGB';
$GLOBALS['TYPO3_CONF_VARS']['GFX']['im'] = 1;
$GLOBALS['TYPO3_CONF_VARS']['GFX']['im_mask_temp_ext_gif'] = 1;
$GLOBALS['TYPO3_CONF_VARS']['GFX']['im_v5effects'] = 1;
$GLOBALS['TYPO3_CONF_VARS']['GFX']['image_processing'] = 1;




# DEBUG / Development Context
$GLOBALS['TYPO3_CONF_VARS']['BE']['debug'] = TRUE;
$GLOBALS['TYPO3_CONF_VARS']['FE']['debug'] = TRUE;
$GLOBALS['TYPO3_CONF_VARS']['SYS']['devIPmask'] = '*';
$GLOBALS['TYPO3_CONF_VARS']['SYS']['displayErrors'] = TRUE;
$GLOBALS['TYPO3_CONF_VARS']['SYS']['enableDeprecationLog'] = 'file';
$GLOBALS['TYPO3_CONF_VARS']['SYS']['sqlDebug'] = 1;
$GLOBALS['TYPO3_CONF_VARS']['SYS']['systemLogLevel'] = 0;
$GLOBALS['TYPO3_CONF_VARS']['SYS']['exceptionalErrors'] = 28674;
$GLOBALS['TYPO3_CONF_VARS']['SYS']['clearCacheSystem'] = TRUE;

# Mail
$GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport'] = 'smtp';
$GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport_sendmail_command'] = '/usr/sbin/sendmail -t -i ';
$GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport_smtp_server'] = 'localhost:1025';